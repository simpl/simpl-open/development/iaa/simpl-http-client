package eu.europa.ec.simpl.client.core;

import static eu.europa.ec.simpl.client.core.util.TestCertificateUtil.anX509Certificate;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CertificateRevocationTrustManagerTest {
    @Mock
    private AuthenticationProviderAdapter authenticationProviderAdapter;

    @Mock
    private X509TrustManager delegate;

    private CertificateRevocationTrustManager certificateRevocationTrustManager;

    @BeforeEach
    public void init() {
        Security.addProvider(new BouncyCastleProvider());
        certificateRevocationTrustManager =
                new CertificateRevocationTrustManager(authenticationProviderAdapter, delegate);
    }

    @Test
    void checkClientTrustedTest() throws CertificateException {
        var certificate = mock(X509Certificate.class);
        X509Certificate[] chain = {certificate};
        String authType = "juni-authtype";
        certificateRevocationTrustManager.checkClientTrusted(chain, authType);
        verify(delegate).checkClientTrusted(eq(chain), eq(authType));
    }

    @Test
    void testCheckServerTrusted() throws CertificateException, NoSuchAlgorithmException, OperatorCreationException {
        var certificate = new JcaX509CertificateConverter()
                .getCertificate(anX509Certificate("CN=OnBoardingCA", "CN=applicant@applicant.com, O=Applicant Entity"));
        X509Certificate[] chain = {certificate};
        String authType = "juni-authtype";
        given(authenticationProviderAdapter.validateCredential(any())).willReturn(true);

        var ex = catchException(() -> certificateRevocationTrustManager.checkServerTrusted(chain, authType));

        assertThat(ex).isNull();
    }

    @Test
    void testCheckServerTrustedWhenValidateCredentialFailShouldThrowCertificateException()
            throws CertificateException, NoSuchAlgorithmException, OperatorCreationException {
        var certificate = new JcaX509CertificateConverter()
                .getCertificate(anX509Certificate("CN=OnBoardingCA", "CN=applicant@applicant.com, O=Applicant Entity"));
        X509Certificate[] chain = {certificate};
        String authType = "juni-authtype";
        given(authenticationProviderAdapter.validateCredential(any())).willReturn(false);

        var ex = catchException(() -> certificateRevocationTrustManager.checkServerTrusted(chain, authType));

        assertThat(ex).as("credential not valid exception").isInstanceOf(CertificateException.class);
    }

    @Test
    void getAcceptedIssuersTest() {
        var certificate = mock(X509Certificate.class);
        X509Certificate[] chain = {certificate};
        when(delegate.getAcceptedIssuers()).thenReturn(chain);
        var result = certificateRevocationTrustManager.getAcceptedIssuers();
        assertThat(result).isEqualTo(chain);
    }
}
