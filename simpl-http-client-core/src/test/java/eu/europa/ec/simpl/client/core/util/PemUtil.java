package eu.europa.ec.simpl.client.core.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.cert.X509Certificate;
import java.util.List;
import lombok.experimental.UtilityClass;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;

@UtilityClass
public class PemUtil {

    public static void writePem(List<X509Certificate> certificateList, OutputStream outputStream) throws IOException {
        try (var pemWriter = new JcaPEMWriter(new OutputStreamWriter(outputStream))) {
            for (var x509Certificate : certificateList) {
                pemWriter.writeObject(x509Certificate);
            }
        }
    }
}
