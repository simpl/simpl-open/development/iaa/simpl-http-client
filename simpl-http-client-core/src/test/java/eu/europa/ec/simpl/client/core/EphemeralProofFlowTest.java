package eu.europa.ec.simpl.client.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class EphemeralProofFlowTest {

    @Mock
    private EphemeralProofAdapter ephemeralProofAdapter;

    @Mock
    private AuthorityUrlSupplier authorityUrlSupplier;

    @Mock
    private EphemeralProofFlow<Object, Object> ephemeralProofFlow;

    @SuppressWarnings("unchecked")
    @BeforeEach
    public void init() throws URISyntaxException {
        ephemeralProofFlow = mock(
                EphemeralProofFlow.class,
                withSettings().useConstructor(ephemeralProofAdapter, authorityUrlSupplier, null));
    }

    @Test
    public void executeTest() {
        doAnswer(CALLS_REAL_METHODS).when(ephemeralProofFlow).execute(any());
        when(ephemeralProofFlow.shouldDoPreflight(any())).thenReturn(true);
        ephemeralProofFlow.execute(null);
    }

    public static Stream<Arguments> extractParticipantBaseUrlTest() {
        return Stream.of(
                Arguments.of("http://host:8080/path", "http://host:8080"),
                Arguments.of("http://host/path", "http://host"),
                Arguments.of("http://host:8080", "http://host:8080"),
                Arguments.of("https://host", "https://host"));
    }

    @MethodSource
    @ParameterizedTest
    public void extractParticipantBaseUrlTest(String uri, String expected) throws URISyntaxException {
        doAnswer(CALLS_REAL_METHODS).when(ephemeralProofFlow).extractParticipantBaseUrl(any());
        when(ephemeralProofFlow.getRequestUrl(any())).thenReturn(URI.create("http://host:8080/path"));
        var baseUrl = ephemeralProofFlow.extractParticipantBaseUrl(null);
        assertThat(baseUrl).isEqualTo("http://host:8080");
    }
}
