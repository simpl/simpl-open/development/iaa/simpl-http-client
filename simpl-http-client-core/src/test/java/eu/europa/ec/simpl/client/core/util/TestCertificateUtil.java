package eu.europa.ec.simpl.client.core.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.instancio.Instancio;

public class TestCertificateUtil {

    private TestCertificateUtil() {
        throw new UnsupportedOperationException("Utility class");
    }

    public static byte[] anEncodedKeystore(String issuer, String subject, KeyPair keyPair)
            throws OperatorCreationException, CertificateException, IOException {
        var cert = new JcaX509CertificateConverter().getCertificate(anX509Certificate(issuer, subject, keyPair));

        var os = new ByteArrayOutputStream();
        PemUtil.writePem(List.of(cert), os);

        return os.toByteArray();
    }

    public static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        return KeyPairGenerator.getInstance("ECDSA").generateKeyPair();
    }

    public static X509CertificateHolder anX509Certificate(String issuer, String subject, KeyPair keyPair)
            throws OperatorCreationException {
        var builder = new X509v3CertificateBuilder(
                new X500Name(issuer),
                BigInteger.valueOf(Instancio.create(Long.class)),
                new Date(Instant.now().toEpochMilli()),
                new Date(Instant.now().plus(365, ChronoUnit.DAYS).toEpochMilli()),
                new X500Name(subject),
                SubjectPublicKeyInfo.getInstance(keyPair.getPublic().getEncoded()));
        return builder.build(new JcaContentSignerBuilder("SHA256withECDSA").build(keyPair.getPrivate()));
    }

    public static X509CertificateHolder anX509Certificate(String issuer, String subject)
            throws NoSuchAlgorithmException, OperatorCreationException {
        return anX509Certificate(issuer, subject, generateKeyPair());
    }
}
