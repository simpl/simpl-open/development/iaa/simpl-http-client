package eu.europa.ec.simpl.client.core;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.ssl.SslInfo;
import eu.europa.ec.simpl.client.core.suppliers.AuthenticationProviderConfigSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.Certificate;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import org.assertj.core.api.Assertions;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SimplClientTest {

    @Mock
    private AuthenticationProviderAdapter authenticationProviderAdapter;

    @Mock
    private CertificateRevocationTrustManagerFactory certificateRevocationTrustManagerFactory;

    private SimplClient simplClient;

    @BeforeEach
    public void init() {
        simplClient = new SimplClient(certificateRevocationTrustManagerFactory);
    }

    @Test
    void buildSslConfigSupplierTest() {

        var keyStore = createKeyStore();
        var sslInfo = new SslInfo(keyStore);
        var hostnameVerifier = mock(HostnameVerifier.class);
        sslInfo.setKeyStorePassword("junit-keystore");
        sslInfo.setHostnameVerifier(hostnameVerifier);

        var ex = Assertions.catchException(
                () -> simplClient.buildSslConfigSupplier(authenticationProviderAdapter, sslInfo));

        Assertions.assertThat(ex).isNull();
    }

    @Test
    void builderTest() {
        var keyStore = createKeyStore();
        var sslInfo = new SslInfo(keyStore);
        var hostnameVerifier = mock(HostnameVerifier.class);
        sslInfo.setKeyStorePassword("junit-keystore");
        sslInfo.setHostnameVerifier(hostnameVerifier);
        var configurator = mock(ClientConfigurator.class);
        var builder = simplClient.builder(configurator);
        var authorizationHeaderSupplier = mock(AuthorizationHeaderSupplier.class);
        var authorityUrlSupplier = mock(AuthorityUrlSupplier.class);
        var ephemeralProofAdapter = mock(EphemeralProofAdapter.class);
        var authenticationProviderConfigSupplier = mock(AuthenticationProviderConfigSupplier.class);

        builder.setSslInfoSupplier(() -> sslInfo);
        builder.setAuthorizationHeaderSupplier(authorizationHeaderSupplier);
        builder.setAuthorityUrlSupplier(authorityUrlSupplier);
        builder.setEphemeralProofAdapter(ephemeralProofAdapter);
        builder.setAuthenticationProviderConfigSupplier(authenticationProviderConfigSupplier);

        when(configurator.configureSsl(any())).thenReturn(configurator);
        when(configurator.configureTierOneTokenPropagator(any())).thenReturn(configurator);
        when(configurator.configureEphemeralProofPreflight(any(), any())).thenReturn(configurator);
        when(configurator.configureEncoder()).thenReturn(configurator);
        when(configurator.configureDecoder()).thenReturn(configurator);
        when(configurator.configureMTLSFallback()).thenReturn(configurator);

        var ex = Assertions.catchException(builder::build);

        Assertions.assertThat(ex).as("Any builder exception").isNull();
    }

    private KeyStore createKeyStore() {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
            var keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(2048);
            var keypair = keyGen.generateKeyPair();
            var cert = generateCertificate(keypair);
            Certificate[] chain = new Certificate[] {cert};
            char[] kp = "junit-keystore".toCharArray();
            String alias = "junitalias";
            keyStore.setKeyEntry(alias, keypair.getPrivate(), kp, chain);

            return keyStore;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Certificate generateCertificate(KeyPair keyPair) {
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            var privateKey = keyPair.getPrivate();
            X500Name issuer = new X500Name("CN=Test Certificate, O=Unit Test, C=US");
            var serialNumber = BigInteger.valueOf(System.currentTimeMillis());
            Date startDate = new Date();
            Date endDate = new Date(System.currentTimeMillis() + (365L * 24 * 60 * 60 * 1000));
            JcaX509v3CertificateBuilder certBuilder = new JcaX509v3CertificateBuilder(
                    issuer, serialNumber, startDate, endDate, issuer, keyPair.getPublic());
            certBuilder.addExtension(
                    Extension.keyUsage, true, new KeyUsage(KeyUsage.digitalSignature | KeyUsage.keyEncipherment));
            certBuilder.addExtension(
                    Extension.subjectAlternativeName,
                    false,
                    new GeneralNames(new GeneralName(GeneralName.rfc822Name, "junit@junit.com")));
            ContentSigner contentSigner = new JcaContentSignerBuilder("SHA256withRSA").build(privateKey);
            X509CertificateHolder certHolder = certBuilder.build(contentSigner);
            return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certHolder);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
