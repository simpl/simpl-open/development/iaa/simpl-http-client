package eu.europa.ec.simpl.client.core;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

public class DaggerSimplClientFactoryTest {

    @Test
    public void builder() {
        assertDoesNotThrow(() -> DaggerSimplClientFactory.builder().build());
    }
}
