package eu.europa.ec.simpl.client.core.suppliers;

import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DefaultSslInfoSupplierTest {

    private DefaultSslInfoSupplier defaultSslInfoSupplier;

    @BeforeEach
    public void init() {
        defaultSslInfoSupplier = new DefaultSslInfoSupplier();
    }

    @Test
    public void getSslInfoTest() throws IOException, KeyStoreException, NoSuchAlgorithmException, CertificateException {
        var tmpKeyStorePwd = "KEYSTORETMPPS";
        var tmpKeyStoreFile = Files.createTempFile("junit", "").toFile();
        var tmpKeyStoreFileOutput = new FileOutputStream(tmpKeyStoreFile);
        var ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, null);
        ks.store(tmpKeyStoreFileOutput, tmpKeyStorePwd.toCharArray());
        try (var defaultSslInfoSupplierClass = mockStatic(DefaultSslInfoSupplier.class, CALLS_REAL_METHODS)) {

            when(DefaultSslInfoSupplier.getenv("CLIENT_KEYSTORE_PATH")).thenReturn(tmpKeyStoreFile.getAbsolutePath());
            when(DefaultSslInfoSupplier.getenv("CLIENT_KEYSTORE_TYPE")).thenReturn(KeyStore.getDefaultType());
            when(DefaultSslInfoSupplier.getenv("CLIENT_KEYSTORE_PASSWORD")).thenReturn(tmpKeyStorePwd);

            defaultSslInfoSupplier.getSslInfo();
        }
    }
}
