package eu.europa.ec.simpl.client.core;

import dagger.assisted.AssistedFactory;
import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import javax.net.ssl.X509TrustManager;

@AssistedFactory
public interface CertificateRevocationTrustManagerFactory {
    CertificateRevocationTrustManager get(
            AuthenticationProviderAdapter authenticationProviderAdapter, X509TrustManager delegate);
}
