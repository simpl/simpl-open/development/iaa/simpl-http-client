package eu.europa.ec.simpl.client.core;

import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.ssl.NoHostnameVerifier;
import eu.europa.ec.simpl.client.core.ssl.SslConfig;
import eu.europa.ec.simpl.client.core.ssl.SslInfo;
import eu.europa.ec.simpl.client.core.suppliers.*;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.net.ssl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class SimplClient {

    private static final Logger log = LoggerFactory.getLogger(SimplClient.class);
    private final CertificateRevocationTrustManagerFactory certificateRevocationTrustManagerFactory;

    @Inject
    protected SimplClient(CertificateRevocationTrustManagerFactory certificateRevocationTrustManagerFactory) {
        this.certificateRevocationTrustManagerFactory = certificateRevocationTrustManagerFactory;
    }

    public <T> Builder<T> builder(ClientConfigurator<T> configurator) {
        return new Builder<>(configurator);
    }

    protected SslConfigSupplier buildSslConfigSupplier(
            AuthenticationProviderAdapter authenticationProviderAdapter, SslInfo sslInfo) {
        try {
            log.info("Building SSL context");
            log.debug("Building keystore");
            var keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(
                    sslInfo.getKeyStore(),
                    Optional.ofNullable(sslInfo.getKeyStorePassword())
                            .map(String::toCharArray)
                            .orElse(null));

            var trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(buildTrustStore(sslInfo.getKeyStore()));
            var trustManager = (X509TrustManager) trustManagerFactory.getTrustManagers()[0];
            var customTrustManager =
                    certificateRevocationTrustManagerFactory.get(authenticationProviderAdapter, trustManager);

            var sslContext = SSLContext.getInstance("TLS");
            sslContext.init(
                    keyManagerFactory.getKeyManagers(), new TrustManager[] {customTrustManager}, new SecureRandom());
            log.info("SSL context built successfully");
            return () -> new SslConfig(sslContext, customTrustManager, getHostnameVerifier(sslInfo));
        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | UnrecoverableKeyException
                | KeyManagementException
                | CertificateException
                | IOException e) {
            throw new IllegalStateException("Error while building SSLContext", e);
        }
    }

    private static KeyStore buildTrustStore(KeyStore keyStore)
            throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException {
        log.debug("Building truststore");
        var alias = keyStore.aliases().nextElement();
        var chain = Arrays.stream(keyStore.getCertificateChain(alias))
                .map(X509Certificate.class::cast)
                .collect(Collectors.toCollection(LinkedList::new));
        chain.removeFirst();
        var trustStore = KeyStore.getInstance("PKCS12");
        trustStore.load(null, null);
        for (X509Certificate cert : chain) {
            keyStore.setCertificateEntry(cert.getSubjectX500Principal().getName(), cert);
        }
        return trustStore;
    }

    private HostnameVerifier getHostnameVerifier(SslInfo sslInfo) {
        return Optional.ofNullable(sslInfo.getHostnameVerifier()).orElse(new NoHostnameVerifier());
    }

    public class Builder<T> {

        private final ClientConfigurator<T> configurator;

        private AuthorizationHeaderSupplier authorizationHeaderSupplier;
        private AuthenticationProviderConfigSupplier authenticationProviderConfigSupplier;

        private AuthorityUrlSupplier authorityUrlSupplier = new EmptyAuthorityUrlSupplier();
        private EphemeralProofAdapter ephemeralProofAdapter = new EmptyEphemeralProofAdapter();

        private SslInfoSupplier sslInfoSupplier = new DefaultSslInfoSupplier();

        private AuthenticationProviderAdapter authenticationProviderAdapter;

        public Builder(ClientConfigurator<T> configurator) {
            this.configurator = configurator;
        }

        public Builder<T> setSslInfoSupplier(SslInfoSupplier sslInfoSupplier) {
            this.sslInfoSupplier = sslInfoSupplier;
            return this;
        }

        public Builder<T> setAuthorizationHeaderSupplier(AuthorizationHeaderSupplier authorizationHeaderSupplier) {
            this.authorizationHeaderSupplier = authorizationHeaderSupplier;
            return this;
        }

        public Builder<T> setAuthenticationProviderConfigSupplier(
                AuthenticationProviderConfigSupplier authenticationProviderConfigSupplier) {
            this.authenticationProviderConfigSupplier = authenticationProviderConfigSupplier;
            return this;
        }

        public Builder<T> setAuthorityUrlSupplier(AuthorityUrlSupplier authorityUrlSupplier) {
            this.authorityUrlSupplier = authorityUrlSupplier;
            return this;
        }

        public Builder<T> setEphemeralProofAdapter(EphemeralProofAdapter ephemeralProofAdapter) {
            this.ephemeralProofAdapter = ephemeralProofAdapter;
            return this;
        }

        public T build() {

            if (authenticationProviderConfigSupplier != null) {
                authenticationProviderAdapter =
                        configurator.buildAuthenticationProviderAdapter(authenticationProviderConfigSupplier);
                ephemeralProofAdapter = new AuthenticationProviderEphemeralProofAdapter(authenticationProviderAdapter);
            } else {
                throw new IllegalArgumentException("authenticationProviderConfigSupplier must be provided");
            }

            if (authenticationProviderAdapter != null && !hasCustomSslInfoSupplier()) {
                sslInfoSupplier = new AuthenticationProviderKeyStoreSupplier(authenticationProviderAdapter);
            }

            return configurator
                    .configureSsl(buildSslConfigSupplier(authenticationProviderAdapter, sslInfoSupplier.getSslInfo()))
                    .configureTierOneTokenPropagator(authorizationHeaderSupplier)
                    .configureEphemeralProofPreflight(authorityUrlSupplier, ephemeralProofAdapter)
                    .configureEncoder()
                    .configureDecoder()
                    .configureMTLSFallback()
                    .getClient();
        }

        private boolean hasCustomSslInfoSupplier() {
            return !(sslInfoSupplier instanceof DefaultSslInfoSupplier);
        }
    }
}
