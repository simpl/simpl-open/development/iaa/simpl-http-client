package eu.europa.ec.simpl.client.core.suppliers;

import eu.europa.ec.simpl.client.core.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.client.core.ssl.SslInfo;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class DefaultSslInfoSupplier implements SslInfoSupplier {

    public static final String CLIENT_KEYSTORE_PATH = "CLIENT_KEYSTORE_PATH";
    public static final String CLIENT_KEYSTORE_TYPE = "CLIENT_KEYSTORE_TYPE";
    public static final String CLIENT_KEYSTORE_PASSWORD = "CLIENT_KEYSTORE_PASSWORD";

    @Override
    public SslInfo getSslInfo() {
        return new SslInfo(loadKeyStore()).setKeyStorePassword(getenv(CLIENT_KEYSTORE_PASSWORD));
    }

    protected static String getenv(String name) {
        return System.getenv(name);
    }

    private static KeyStore loadKeyStore() {
        return loadKeyStore(
                getenv(CLIENT_KEYSTORE_PATH), getenv(CLIENT_KEYSTORE_TYPE), getenv(CLIENT_KEYSTORE_PASSWORD));
    }

    private static KeyStore loadKeyStore(String path, String type, String password) {
        try (var file = new FileInputStream(path)) {
            var keyStore = KeyStore.getInstance(type);
            keyStore.load(file, password.toCharArray());
            return keyStore;
        } catch (CertificateException | IOException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new RuntimeWrapperException(e);
        }
    }
}
