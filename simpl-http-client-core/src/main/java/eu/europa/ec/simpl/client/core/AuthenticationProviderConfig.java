package eu.europa.ec.simpl.client.core;

import java.net.URI;

public record AuthenticationProviderConfig(URI url) {}
