package eu.europa.ec.simpl.client.core;

public abstract class AbstractClientConfigurator<T> implements ClientConfigurator<T> {

    private final T client;

    protected AbstractClientConfigurator(T client) {
        this.client = client;
    }

    @Override
    public T getClient() {
        return client;
    }
}
