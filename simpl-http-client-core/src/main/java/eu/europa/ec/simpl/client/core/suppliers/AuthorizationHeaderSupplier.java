package eu.europa.ec.simpl.client.core.suppliers;

@FunctionalInterface
public interface AuthorizationHeaderSupplier {
    String getAuthorizationHeader();
}
