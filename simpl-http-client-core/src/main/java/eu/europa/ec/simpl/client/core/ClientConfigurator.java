package eu.europa.ec.simpl.client.core;

import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.suppliers.AuthenticationProviderConfigSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import eu.europa.ec.simpl.client.core.suppliers.SslConfigSupplier;

public interface ClientConfigurator<T> {

    AuthenticationProviderAdapter buildAuthenticationProviderAdapter(
            AuthenticationProviderConfigSupplier authenticationProviderConfigSupplier);

    ClientConfigurator<T> configureSsl(SslConfigSupplier sslConfigSupplier);

    ClientConfigurator<T> configureTierOneTokenPropagator(AuthorizationHeaderSupplier authorizationHeaderSupplier);

    ClientConfigurator<T> configureEphemeralProofPreflight(
            AuthorityUrlSupplier authorityUrlSupplier, EphemeralProofAdapter ephemeralProofRepository);

    ClientConfigurator<T> configureMTLSFallback();

    ClientConfigurator<T> configureEncoder();

    ClientConfigurator<T> configureDecoder();

    T getClient();
}
