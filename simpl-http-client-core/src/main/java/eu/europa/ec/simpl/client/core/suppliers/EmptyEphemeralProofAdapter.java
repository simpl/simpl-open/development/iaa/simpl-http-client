package eu.europa.ec.simpl.client.core.suppliers;

import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmptyEphemeralProofAdapter implements EphemeralProofAdapter {

    private static final Logger log = LoggerFactory.getLogger(EmptyEphemeralProofAdapter.class);

    @Override
    public Optional<String> loadEphemeralProof() {
        log.warn(
                "Empty implementation, a new ephemeral proof will be requested to the governance authority if the authority url is configured");
        return Optional.empty();
    }

    @Override
    public void storeEphemeralProof(String ephemeralProof) {
        log.warn("Empty implementation, ephemeral proof will not be cached!");
    }
}
