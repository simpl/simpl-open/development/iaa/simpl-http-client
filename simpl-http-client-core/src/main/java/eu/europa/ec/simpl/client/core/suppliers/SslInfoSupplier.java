package eu.europa.ec.simpl.client.core.suppliers;

import eu.europa.ec.simpl.client.core.ssl.SslInfo;

@FunctionalInterface
public interface SslInfoSupplier {

    SslInfo getSslInfo();
}
