package eu.europa.ec.simpl.client.core.adapters;

import java.util.Optional;

public interface EphemeralProofAdapter {

    Optional<String> loadEphemeralProof();

    void storeEphemeralProof(String ephemeralProof);
}
