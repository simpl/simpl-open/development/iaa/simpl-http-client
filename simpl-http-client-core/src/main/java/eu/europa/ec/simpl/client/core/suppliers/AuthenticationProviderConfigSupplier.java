package eu.europa.ec.simpl.client.core.suppliers;

import eu.europa.ec.simpl.client.core.AuthenticationProviderConfig;

public interface AuthenticationProviderConfigSupplier {

    AuthenticationProviderConfig getAuthenticationProviderConfig();
}
