package eu.europa.ec.simpl.client.core.adapters;

import java.security.KeyStore;

public interface AuthenticationProviderAdapter {

    KeyStore getKeyStore();

    String getEphemeralProof();

    boolean validateCredential(String credential);
}
