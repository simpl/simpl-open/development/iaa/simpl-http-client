package eu.europa.ec.simpl.client.core;

import dagger.Component;
import javax.inject.Singleton;

@Singleton
@Component
public interface SimplClientFactory {
    SimplClient get();
}
