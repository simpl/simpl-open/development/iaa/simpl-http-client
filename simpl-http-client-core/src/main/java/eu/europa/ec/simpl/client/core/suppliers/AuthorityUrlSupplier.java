package eu.europa.ec.simpl.client.core.suppliers;

@FunctionalInterface
public interface AuthorityUrlSupplier {

    String getAuthorityUrl();
}
