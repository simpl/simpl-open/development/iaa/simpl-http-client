package eu.europa.ec.simpl.client.core.suppliers;

import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import eu.europa.ec.simpl.client.core.ssl.SslInfo;

public class AuthenticationProviderKeyStoreSupplier implements SslInfoSupplier {
    private final AuthenticationProviderAdapter authenticationProviderAdapter;

    public AuthenticationProviderKeyStoreSupplier(AuthenticationProviderAdapter authenticationProviderAdapter) {
        this.authenticationProviderAdapter = authenticationProviderAdapter;
    }

    @Override
    public SslInfo getSslInfo() {
        return new SslInfo(authenticationProviderAdapter.getKeyStore());
    }
}
