package eu.europa.ec.simpl.client.core;

import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class EphemeralProofFlow<T, R> {

    private static final Logger log = LoggerFactory.getLogger(EphemeralProofFlow.class);

    protected final EphemeralProofAdapter ephemeralProofAdapter;
    protected final AuthorityUrlSupplier authorityUrlSupplier;
    protected final T client;

    protected EphemeralProofFlow(
            EphemeralProofAdapter ephemeralProofAdapter, AuthorityUrlSupplier authorityUrlSupplier, T client) {
        this.ephemeralProofAdapter = ephemeralProofAdapter;
        this.authorityUrlSupplier = authorityUrlSupplier;
        this.client = client;
    }

    protected abstract boolean shouldDoPreflight(R request);

    protected abstract void sendEphemeralProofToPeer(String participantUrl, String ephemeralProof);

    protected abstract String getEphemeralProofFromAuthority();

    protected abstract URI getRequestUrl(R request) throws URISyntaxException;

    public void execute(R request) {
        if (shouldDoPreflight(request)) {
            log.info("fetching cached ephemeral proof");
            var ephemeralProof = ephemeralProofAdapter.loadEphemeralProof().or(this::getProofFromAuthorityAndStore);
            String participantUrl = null;
            try {
                if (ephemeralProof.isPresent()) {
                    participantUrl = extractParticipantBaseUrl(request);
                    log.info("Preflight ephemeral proof to: {}", participantUrl);
                    sendEphemeralProofToPeer(participantUrl, ephemeralProof.get());
                } else {
                    log.warn("Preflight skipped: cloud not obtain ephemeral proof");
                }
            } catch (Exception e) {
                log.error("Unable to send ephemeral proof to peer {}", participantUrl, e);
            }
        }
    }

    private Optional<String> getProofFromAuthorityAndStore() {

        return Optional.ofNullable(authorityUrlSupplier.getAuthorityUrl())
                .map(authorityUrl -> {
                    log.info("ephemeral proof not present, start fetching from authority {}", authorityUrl);
                    var ep = getEphemeralProofFromAuthority();
                    ephemeralProofAdapter.storeEphemeralProof(ep);
                    return ep;
                })
                .or(() -> {
                    log.info("authority url not present, skipping fetch from authority");
                    return Optional.empty();
                });
    }

    protected String extractParticipantBaseUrl(R request) throws URISyntaxException {
        var uri = getRequestUrl(request);
        return "%s://%s%s".formatted(uri.getScheme(), uri.getHost(), getPortIfPresent(uri));
    }

    private String getPortIfPresent(URI uri) {
        return uri.getPort() != -1 ? ":%d".formatted(uri.getPort()) : "";
    }
}
