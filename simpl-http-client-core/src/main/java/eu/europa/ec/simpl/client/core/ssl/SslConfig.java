package eu.europa.ec.simpl.client.core.ssl;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@RequiredArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SslConfig {
    @NonNull SSLContext sslContext;

    @NonNull X509TrustManager trustManager;

    HostnameVerifier hostnameVerifier;
}
