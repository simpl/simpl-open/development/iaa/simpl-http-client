package eu.europa.ec.simpl.client.core.ssl;

import java.security.KeyStore;
import javax.net.ssl.HostnameVerifier;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@Accessors(chain = true)
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SslInfo {
    @NonNull KeyStore keyStore;

    String keyStorePassword;
    HostnameVerifier hostnameVerifier;
}
