package eu.europa.ec.simpl.client.core.suppliers;

public class EmptyAuthorityUrlSupplier implements AuthorityUrlSupplier {
    @Override
    public String getAuthorityUrl() {
        return null;
    }
}
