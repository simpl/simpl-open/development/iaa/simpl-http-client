package eu.europa.ec.simpl.client.core.ssl;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class NoHostnameVerifier implements HostnameVerifier {
    @Override
    @SuppressWarnings("java:S5527") // we intentionally override this check
    public boolean verify(String s, SSLSession sslSession) {
        return true;
    }
}
