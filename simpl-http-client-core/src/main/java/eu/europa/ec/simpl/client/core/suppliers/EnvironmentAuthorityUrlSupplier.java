package eu.europa.ec.simpl.client.core.suppliers;

public class EnvironmentAuthorityUrlSupplier implements AuthorityUrlSupplier {
    @Override
    public String getAuthorityUrl() {
        return System.getenv("CLIENT_AUTHORITY_URL");
    }
}
