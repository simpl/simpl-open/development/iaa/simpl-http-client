package eu.europa.ec.simpl.client.core;

import dagger.assisted.Assisted;
import dagger.assisted.AssistedInject;
import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.X509TrustManager;
import lombok.SneakyThrows;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;

public class CertificateRevocationTrustManager implements X509TrustManager {
    private final AuthenticationProviderAdapter authenticationProviderAdapter;
    private final X509TrustManager delegate;

    @AssistedInject
    CertificateRevocationTrustManager(
            @Assisted AuthenticationProviderAdapter authenticationProviderAdapter,
            @Assisted X509TrustManager delegate) {
        this.authenticationProviderAdapter = authenticationProviderAdapter;
        this.delegate = delegate;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        delegate.checkClientTrusted(chain, authType);
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        var certificate = chain[0];
        var outputStream = new ByteArrayOutputStream();
        writePem(List.of(certificate), outputStream);
        var pem = outputStream.toString();
        if (!authenticationProviderAdapter.validateCredential(pem)) {
            throw new CertificateException("Certificate not valid or revoked");
        }
    }

    @SneakyThrows
    public static void writePem(List<X509Certificate> certificateList, OutputStream outputStream) {
        try (var pemWriter = new JcaPEMWriter(new OutputStreamWriter(outputStream))) {
            for (var x509Certificate : certificateList) {
                pemWriter.writeObject(x509Certificate);
            }
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return delegate.getAcceptedIssuers();
    }
}
