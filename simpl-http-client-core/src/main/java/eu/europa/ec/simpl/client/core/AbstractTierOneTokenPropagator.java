package eu.europa.ec.simpl.client.core;

import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;

public abstract class AbstractTierOneTokenPropagator {

    private final AuthorizationHeaderSupplier authorizationHeaderSupplier;

    protected AbstractTierOneTokenPropagator(AuthorizationHeaderSupplier authorizationHeaderSupplier) {
        this.authorizationHeaderSupplier = authorizationHeaderSupplier;
    }

    protected String getAuthorizationHeader() {
        return authorizationHeaderSupplier.getAuthorizationHeader();
    }
}
