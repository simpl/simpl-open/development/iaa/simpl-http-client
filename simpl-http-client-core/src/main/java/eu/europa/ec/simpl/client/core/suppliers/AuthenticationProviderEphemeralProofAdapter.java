package eu.europa.ec.simpl.client.core.suppliers;

import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import java.util.Optional;

public class AuthenticationProviderEphemeralProofAdapter implements EphemeralProofAdapter {

    private final AuthenticationProviderAdapter authenticationProviderAdapter;

    public AuthenticationProviderEphemeralProofAdapter(AuthenticationProviderAdapter authenticationProviderAdapter) {
        this.authenticationProviderAdapter = authenticationProviderAdapter;
    }

    @Override
    public Optional<String> loadEphemeralProof() {
        return Optional.of(authenticationProviderAdapter.getEphemeralProof());
    }

    @Override
    public void storeEphemeralProof(String ephemeralProof) {
        // The ephemeral proof is always fetched from authentication provider, which should handle caching
    }
}
