package eu.europa.ec.simpl.client.core.suppliers;

import eu.europa.ec.simpl.client.core.ssl.SslConfig;

@FunctionalInterface
public interface SslConfigSupplier {

    SslConfig getSslConfig();
}
