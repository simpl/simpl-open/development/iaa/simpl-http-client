package eu.europa.ec.simpl.client.core.exceptions;

public class RuntimeWrapperException extends RuntimeException {

    public RuntimeWrapperException(Exception e) {
        super(e);
    }
}
