# SimplClient

The `simpl-http-client` is a Java utility library for building secure clients that support HTTP/2 communication and custom SSL/TLS configurations. It also integrates certificate revocation checks for enhanced security and performs ephemeral proof preflight request for endpoints that require an ephemeral proof.

The library is client agnostic, meaning that the client can be based on any http client library, such as Feign or OkHttp.

## Table of Contents

- [Installation](#installation)
- [Overview](#overview)
- [Usage](#usage)
    - [Step 1: Instantiate SimplClient](#step-1-instantiate-simplclient)
    - [Step 2: Configure your SimplClient](#step-2-configure-your-simplclient)

## Installation

To use `simpl-http-client` in a Maven project, add the following dependency to your `pom.xml`:

```xml
<dependency>
    <groupId>eu.europa.ec.simpl</groupId>
    <artifactId>simpl-http-client-core</artifactId>
    <version>1.0.0</version>
</dependency>
```

Please provide also the repository location:

```xml
<repositories>
    <repository>
        <id>http-client-maven</id>
        <url>https://code.europa.eu/api/v4/projects/859/packages/maven</url>
    </repository>
</repositories>
```

## Overview

The `SimplClient` class offers a robust way to build Feign clients with enhanced security features:

- **Custom SSL/TLS Configuration**: Establish secure communication using client and server certificates.
- **HTTP/2 Support**: Utilizes Feign's `Http2Client` to improve performance with HTTP/2 servers.
- **Certificate Revocation Checks**: Verifies that SSL certificates are not revoked during the SSL handshake, adding an extra layer of security.
- **Tier One Session Propagation**: Propagate your own tier one session to the mTLS connection

### Key Features

- **Custom SSL Context**: Configures SSL/TLS settings using your KeyStore (for client certificates) and TrustStore (for trusted CA certificates).
- **Jackson Encoder**: Supports JSON serialization and deserialization, including Java 8 date/time types.
- **SLF4J Logging**: Provides detailed logging of HTTP requests and responses, useful for debugging and auditing.

## Usage

### Step 1: Instantiate SimplClient

Use the dependency injection system to instantiate the SimplClient:

```java
@Inject
SimplClient simplClient;
```
Or the Dagger-generated factory:

```java
SimplClient simplClient = DaggerSimplClientFactory.create().get();
```

### Step 2: Configure your SimplClient

The `SimplClient` wraps an existing client instance adding the desired behavior. The behavior is implemented by a `ClientConfigurator` instance that handles client-specific configurations.

Implementations of `ClientConfigurator` and client-specific factories are provided for the following client libraries:

* Feign (in project `simpl-http-client-feign`)
* OkHttp (in project `simpl-http-client-okhttp`)

Please refer to the `README.md` files in the corresponding projects for examples of specific clients' configurations.