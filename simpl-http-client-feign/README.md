# SimplClient

The `simpl-http-client-feign` is a binding of the `simpl-http-client` to the Feign client library.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
    - [Step 1: Instantiate FeignSimplClient](#step-1-instantiate-feignsimplclient)
    - [Step 2: Configure your FeignSimplClient](#step-2-configure-your-feignsimplclient)
    - [Examples](#examples)

## Installation

To use `simpl-http-client-feign` in a Maven project, add the following dependency to your `pom.xml`:

```xml
<dependency>
    <groupId>eu.europa.ec.simpl</groupId>
    <artifactId>simpl-http-client-feign</artifactId>
    <version>1.0.0</version>
</dependency>
```

Please provide also the repository location:

```xml
<repositories>
    <repository>
        <id>http-client-maven</id>
        <url>https://code.europa.eu/api/v4/projects/859/packages/maven</url>
    </repository>
</repositories>
```
## Usage

### Step 1: Instantiate FeignSimplClient

Use the dependency injection system to instantiate the `FeignSimplClient`:

```java
@Inject
FeignSimplClient simplClient;
```

Or the Dagger-generated factory:

```java
FeignSimplClient simplClient = DaggerFeignSimplClientFactory.create().get();
```

### Step 2: Configure your FeignSimplClient

The `FeignSimplClient` can be configured using a fluent builder syntax or via environment variables.

#### Environment variables

The library requires the configurations provided via the following environment variables, or via the equivalent builder API method.

| Variable                     | Description                                                               | Builder API Method        |
|------------------------------|---------------------------------------------------------------------------|---------------------------|
| `CLIENT_AUTHORITY_URL`       | The authority URL, used to perform the ephemeral proof preflight request. | `setAuthorityUrlSupplier` |
| `CLIENT_KEYSTORE_PATH`       | The path where to load the KeyStore file.                                 | `setSslInfoSupplier`      |
| `CLIENT_KEYSTORE_TYPE`       | The KeyStore's type.                                                      | `setSslInfoSupplier`      |
| `CLIENT_KEYSTORE_PASSWORD`   | The KeyStore's password.                                                  | `setSslInfoSupplier`      |

### Examples

#### Example 1: configuration via environment variables

By invoking the `build()` method, the library will instantiate a default implementation that uses configurations from environment variables.

```java

import feign.Feign;

Feign.Builder builder = simplClient.build();
MyApiClient apiClient = builder.target(MyApiClient.class, "https://peer-tls-domain");
```

#### Example 2: configuration via the builder API 

The builder API of `FeignSimplClient` allows to configure several behaviors:

* `SslInfoSupplier`: provides a method to obtain the `SslInfo` object containing Keystore and TrustStore for the client and the KeyStore's password
* `AuthorizationHeaderSupplier`: provides a method to obtain the `Authorization` header value to be propagated on each request, if needed
* `EphemeralProofAdapter`: provides methods to load and store the ephemeral proof obtained during the ephemeral proof prefetch flow
* `AuthorityUrlSupplier`: provides a method to obtain the authority url where to perform the ephemeral proof preflight request

```java
import eu.europa.ec.simpl.client.ssl.SslInfo;
import feign.Feign;

SslInfo sslInfo = new SslInfo(keyStore)
        .setKeyStorePassword("your password"); //only if your keystore has a password

Feign.Builder builder = simplClient
        .builder()
        .setSslInfoSupplier(() -> sslInfo)
        .setAuthorizationHeaderSupplier(tokenPropagator)
        .setEphemeralProofAdapter(ephemeralProofAdapter)
        .setAuthorityUrlSupplier(() -> clientProperties.authority().url())
        .build()
        .target(clazz, url);

MyApiClient apiClient = builder.target(MyApiClient.class, "https://your-tls-domain");
```

`MyApiClient` should be an interface annotated with Feign annotations to define the API endpoints.

#### Example 3: loading KeyStore

Load a java Keystore using a method like the one that you can find at this url:

https://code.europa.eu/simpl/simpl-open/development/iaa/common/-/blob/v0.7.0/simpl-spring-boot-starter/src/main/java/com/aruba/simpl/common/utils/CredentialUtil.java?ref_type=tags#L35
