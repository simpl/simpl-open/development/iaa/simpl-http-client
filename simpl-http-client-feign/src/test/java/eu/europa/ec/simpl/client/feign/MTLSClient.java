package eu.europa.ec.simpl.client.feign;

import feign.RequestLine;

public interface MTLSClient {
    @RequestLine("POST /identity-api/mtls/token")
    String token();
}
