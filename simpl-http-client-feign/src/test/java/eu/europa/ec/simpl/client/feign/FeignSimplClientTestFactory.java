package eu.europa.ec.simpl.client.feign;

import dagger.Component;
import dagger.Module;
import dagger.Provides;
import eu.europa.ec.simpl.client.util.CertificateRevocation;
import javax.inject.Singleton;

@Singleton
@Component(modules = FeignSimplClientTestFactory.TestModule.class)
public interface FeignSimplClientTestFactory {
    FeignSimplClient get();

    @Module
    interface TestModule {
        @Provides
        static CertificateRevocation provideMockedCertificateRevocation() {
            return MockPool.build(CertificateRevocation.class);
        }
    }
}
