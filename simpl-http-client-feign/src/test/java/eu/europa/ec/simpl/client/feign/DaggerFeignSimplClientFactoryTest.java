package eu.europa.ec.simpl.client.feign;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

public class DaggerFeignSimplClientFactoryTest {

    @Test
    public void buildTest() {
        assertDoesNotThrow(() -> DaggerFeignSimplClientFactory.builder().build());
    }

    @Test
    public void getTest() {
        assertDoesNotThrow(() -> DaggerFeignSimplClientFactory.builder().build().get());
    }
}
