package eu.europa.ec.simpl.client.feign;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.doThrow;

import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import eu.europa.ec.simpl.client.core.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.client.core.ssl.SslInfo;
import eu.europa.ec.simpl.client.core.suppliers.SslInfoSupplier;
import eu.europa.ec.simpl.client.util.CertificateRevocation;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import org.instancio.junit.InstancioSource;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@Disabled
@ExtendWith(MockitoExtension.class)
class FeignSimplClientTest {
    @RegisterExtension
    static WireMockExtension wm = wiremockExtension();

    static WireMockExtension wiremockExtension() {
        var trustStorePath = getPath("server-truststore.p12").toString();
        var keyStorePath = getPath("server-keystore.p12").toString();
        return WireMockExtension.newInstance()
                .options(wireMockConfig()
                        .dynamicHttpsPort()
                        .httpDisabled(true)
                        .trustStorePath(trustStorePath)
                        .keystorePath(keyStorePath)
                        .needClientAuth(true))
                .configureStaticDsl(true)
                .build();
    }

    @ParameterizedTest
    @InstancioSource
    void mtlsClientTest(String token) {
        stubFor(post(urlPathTemplate("/identity-api/mtls/token"))
                .willReturn(ok(token).withHeader("Content-Type", "text/plain;charset=UTF-8")));
        var client = buildClient();
        var result = client.token();
        assertThat(result).isEqualTo(token);
    }

    @Test
    void client_whenMTLSFails_shouldUseFallback() throws CertificateException {
        var client = buildClient();
        var certificateRevocation = MockPool.get(CertificateRevocation.class);
        doThrow(new CertificateException("test"))
                .when(certificateRevocation)
                .verify(Mockito.any(X509Certificate.class));
        var exception = catchException(client::token);
        if (exception != null) {
            assertThat(exception).rootCause().isNotInstanceOf(CertificateException.class);
        }
    }

    private MTLSClient buildClient() {
        var url = wm.getRuntimeInfo().getHttpsBaseUrl();

        return DaggerFeignSimplClientTestFactory.create()
                .get()
                .builder()
                .setSslInfoSupplier(getSslInfoSupplier())
                .build()
                .target(MTLSClient.class, url);
    }

    private static @NotNull SslInfoSupplier getSslInfoSupplier() {
        var keyStore = getKeyStore("client-keystore.p12");
        return () -> new SslInfo(keyStore).setKeyStorePassword("password");
    }

    private static KeyStore getKeyStore(String path) {
        try {
            var keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(Files.newInputStream(getPath(path)), "password".toCharArray());
            return keyStore;
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    private static Path getPath(String path) {
        try {
            return Path.of(FeignSimplClientTest.class
                    .getClassLoader()
                    .getResource(path)
                    .toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
