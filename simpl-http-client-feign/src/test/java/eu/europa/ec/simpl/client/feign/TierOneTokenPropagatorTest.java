package eu.europa.ec.simpl.client.feign;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import feign.RequestTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TierOneTokenPropagatorTest {

    @Mock
    private AuthorizationHeaderSupplier authorizationHeaderSupplier;

    private TierOneTokenPropagator tierOneTokenPropagator;

    @BeforeEach
    public void init() {
        tierOneTokenPropagator = new TierOneTokenPropagator(authorizationHeaderSupplier);
    }

    @Test
    public void apply_whenAuthorizationIsNotNull_thenSetHeader() {
        var requestTemplate = mock(RequestTemplate.class);
        var auth = "junit-auth";
        when(authorizationHeaderSupplier.getAuthorizationHeader()).thenReturn(auth);
        tierOneTokenPropagator.apply(requestTemplate);
        verify(requestTemplate).header(eq("Authorization"), eq(auth));
    }

    @Test
    public void apply_whenAuthorizationIsNull_thenDontSetHeader() {
        var requestTemplate = mock(RequestTemplate.class);
        String auth = null;
        when(authorizationHeaderSupplier.getAuthorizationHeader()).thenReturn(auth);
        tierOneTokenPropagator.apply(requestTemplate);
        verify(requestTemplate, times(0)).header(any());
    }

    @Test
    public void apply_whenAuthorizationIsEmpty_thenDontSetHeader() {
        var requestTemplate = mock(RequestTemplate.class);
        var auth = "";
        when(authorizationHeaderSupplier.getAuthorizationHeader()).thenReturn(auth);
        tierOneTokenPropagator.apply(requestTemplate);
        verify(requestTemplate, times(0)).header(any());
    }
}
