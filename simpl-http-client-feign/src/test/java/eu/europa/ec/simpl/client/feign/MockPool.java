package eu.europa.ec.simpl.client.feign;

import java.util.HashMap;
import java.util.Map;
import org.mockito.Mockito;

public class MockPool {
    private static final Map<Class<?>, Object> mocks = new HashMap<>();

    public static <T> T build(Class<T> clazz) {
        mocks.put(clazz, Mockito.mock(clazz));
        return get(clazz);
    }

    public static <T> T get(Class<T> clazz) {
        return (T) mocks.get(clazz);
    }
}
