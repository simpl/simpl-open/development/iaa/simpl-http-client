package eu.europa.ec.simpl.client.feign;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import feign.RequestTemplate;
import feign.codec.Encoder;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CustomEncoderTest {

    @Mock
    private RequestTemplate requestTemplate;

    private CustomEncoder customEncoder;

    @Mock
    private Encoder jacksonEncoder;

    @Mock
    private Encoder defaultEncoder;

    @BeforeEach
    public void init() {
        customEncoder = new CustomEncoder(jacksonEncoder, defaultEncoder);
    }

    @Test
    public void encode_withoutTextPlain_useJacksonEncoder() {
        var object = "object";
        var bodyType = mock(Type.class);
        var template = mock(RequestTemplate.class);
        Map<String, Collection<String>> headers = Map.of("Content-Type", List.of("application/json"));
        when(template.headers()).thenReturn(headers);
        customEncoder.encode(object, bodyType, template);
        verify(jacksonEncoder).encode(object, bodyType, template);
    }

    @Test
    public void encode_withTextPlain_useDefaultEncoder() {
        var object = "object";
        var bodyType = mock(Type.class);
        var template = mock(RequestTemplate.class);
        Map<String, Collection<String>> headers = Map.of("Content-Type", List.of("text/plain"));
        when(template.headers()).thenReturn(headers);
        customEncoder.encode(object, bodyType, template);
        verify(defaultEncoder).encode(object, bodyType, template);
    }
}
