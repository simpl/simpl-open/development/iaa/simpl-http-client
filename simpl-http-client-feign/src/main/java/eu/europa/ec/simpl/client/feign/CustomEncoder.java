package eu.europa.ec.simpl.client.feign;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import feign.RequestTemplate;
import feign.codec.EncodeException;
import feign.codec.Encoder;
import feign.jackson.JacksonEncoder;
import java.lang.reflect.Type;
import java.util.List;

public class CustomEncoder implements Encoder {

    private static final String CONTENT_TYPE_HEADER = "content-type";
    private static final String TEXT_PLAIN_VALUE = "text/plain";

    private final Encoder jacksonEncoder;
    private final Encoder defaultEncoder;

    protected CustomEncoder(Encoder jacksonEncoder, Encoder defaultEncoder) {
        this.jacksonEncoder = jacksonEncoder;
        this.defaultEncoder = defaultEncoder;
    }

    protected CustomEncoder() {
        this(new JacksonEncoder(List.of(new JavaTimeModule())), new Encoder.Default());
    }

    @Override
    public void encode(Object object, Type bodyType, RequestTemplate template) throws EncodeException {
        if (template.headers().entrySet().stream()
                .filter(e -> e.getKey().equalsIgnoreCase(CONTENT_TYPE_HEADER))
                .flatMap(e -> e.getValue().stream())
                .map(String::toLowerCase)
                .anyMatch(h -> h.startsWith(TEXT_PLAIN_VALUE))) {
            defaultEncoder.encode(object, bodyType, template);
        } else {
            jacksonEncoder.encode(object, bodyType, template);
        }
    }
}
