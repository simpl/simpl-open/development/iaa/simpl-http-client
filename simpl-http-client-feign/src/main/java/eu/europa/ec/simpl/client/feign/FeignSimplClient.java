package eu.europa.ec.simpl.client.feign;

import eu.europa.ec.simpl.client.core.SimplClient;
import feign.Feign;
import feign.Logger;
import feign.Retryer;
import feign.slf4j.Slf4jLogger;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class FeignSimplClient {

    private final SimplClient simplClient;

    @Inject
    public FeignSimplClient(SimplClient simplClient) {
        this.simplClient = simplClient;
    }

    public SimplClient.Builder<Feign.Builder> builder(Feign.Builder builder) {
        return simplClient.builder(new FeignClientConfigurator(builder));
    }

    public SimplClient.Builder<Feign.Builder> builder() {
        return builder(createDefaultFeignClientBuilder());
    }

    public Feign.Builder build() {
        return builder().build();
    }

    private static Feign.Builder createDefaultFeignClientBuilder() {
        return Feign.builder()
                .retryer(Retryer.NEVER_RETRY)
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL);
    }
}
