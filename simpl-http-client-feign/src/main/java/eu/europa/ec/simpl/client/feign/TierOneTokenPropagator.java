package eu.europa.ec.simpl.client.feign;

import eu.europa.ec.simpl.client.core.AbstractTierOneTokenPropagator;
import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import feign.RequestInterceptor;
import feign.RequestTemplate;

public class TierOneTokenPropagator extends AbstractTierOneTokenPropagator implements RequestInterceptor {

    protected TierOneTokenPropagator(AuthorizationHeaderSupplier authorizationHeaderSupplier) {
        super(authorizationHeaderSupplier);
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        var authorization = getAuthorizationHeader();
        if (authorization != null && !authorization.isBlank()) {
            requestTemplate.header("Authorization", authorization);
        }
    }
}
