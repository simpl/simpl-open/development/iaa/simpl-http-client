package eu.europa.ec.simpl.client.feign.exchanges;

import feign.Headers;
import feign.RequestLine;

public interface ParticipantEphemeralProofExchange {

    @RequestLine("POST /user-api/mtls/ephemeral-proof")
    @Headers("Content-Type: text/plain")
    void sendEphemeralProof(String ephemeralProof);
}
