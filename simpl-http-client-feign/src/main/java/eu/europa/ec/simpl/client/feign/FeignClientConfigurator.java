package eu.europa.ec.simpl.client.feign;

import eu.europa.ec.simpl.client.core.AbstractClientConfigurator;
import eu.europa.ec.simpl.client.core.ClientConfigurator;
import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.suppliers.AuthenticationProviderConfigSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import eu.europa.ec.simpl.client.core.suppliers.SslConfigSupplier;
import eu.europa.ec.simpl.client.feign.inteceptors.EphemeralProofInterceptor;
import feign.Client;
import feign.Feign;
import feign.Request;
import feign.Response;
import feign.http2client.Http2Client;
import java.io.IOException;
import javax.net.ssl.SSLHandshakeException;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeignClientConfigurator extends AbstractClientConfigurator<Feign.Builder> {

    private final DelegatingClient delegatingClient;

    public FeignClientConfigurator(Feign.Builder client) {
        super(client);
        delegatingClient = new DelegatingClient();
        client.client(delegatingClient);
    }

    @Override
    public AuthenticationProviderAdapter buildAuthenticationProviderAdapter(
            AuthenticationProviderConfigSupplier authenticationProviderConfigSupplier) {
        // TODO Entire configurator will be removed
        throw new UnsupportedOperationException(
                "Feign Client should be removed and reduced to a wrapper of the OkHttp Client");
    }

    @Override
    public ClientConfigurator<Feign.Builder> configureSsl(SslConfigSupplier sslConfigSupplier) {
        var okHttpClient = new OkHttpClient.Builder()
                .sslSocketFactory(
                        sslConfigSupplier.getSslConfig().getSslContext().getSocketFactory(),
                        sslConfigSupplier.getSslConfig().getTrustManager())
                .hostnameVerifier(sslConfigSupplier.getSslConfig().getHostnameVerifier())
                .build();
        delegatingClient.setMtlsClient(new feign.okhttp.OkHttpClient(okHttpClient));
        return this;
    }

    @Override
    public ClientConfigurator<Feign.Builder> configureTierOneTokenPropagator(
            @Nullable AuthorizationHeaderSupplier authorizationHeaderSupplier) {
        if (authorizationHeaderSupplier != null) {
            getClient().requestInterceptor(new TierOneTokenPropagator(authorizationHeaderSupplier));
        }
        return this;
    }

    @Override
    public ClientConfigurator<Feign.Builder> configureEphemeralProofPreflight(
            AuthorityUrlSupplier authorityUrlSupplier, EphemeralProofAdapter ephemeralProofAdapter) {

        if (ephemeralProofAdapter != null) {
            getClient()
                    .requestInterceptor(
                            new EphemeralProofInterceptor(ephemeralProofAdapter, authorityUrlSupplier, getClient()));
        }
        return this;
    }

    @Override
    public ClientConfigurator<Feign.Builder> configureMTLSFallback() {
        delegatingClient.setFallbackClient(new Http2Client());
        return this;
    }

    @Override
    public ClientConfigurator<Feign.Builder> configureEncoder() {
        getClient().encoder(new CustomEncoder());
        return this;
    }

    @Override
    public ClientConfigurator<Feign.Builder> configureDecoder() {
        getClient().decoder(new CustomDecoder());
        return this;
    }

    private static class DelegatingClient implements Client {

        private static final Logger log = LoggerFactory.getLogger(DelegatingClient.class);

        private Client mtlsClient;
        private Client fallbackClient;

        @Override
        public Response execute(Request request, Request.Options options) throws IOException {
            try {
                return mtlsClient.execute(request, options);
            } catch (SSLHandshakeException e) {
                log.error(
                        "Exception while attempting mTLS request {}, falling back to backup client",
                        "%s %s".formatted(request.httpMethod(), request.url()),
                        e);
                return fallbackClient.execute(request, options);
            }
        }

        public DelegatingClient setMtlsClient(Client mtlsClient) {
            this.mtlsClient = mtlsClient;
            return this;
        }

        public DelegatingClient setFallbackClient(Client fallbackClient) {
            this.fallbackClient = fallbackClient;
            return this;
        }
    }
}
