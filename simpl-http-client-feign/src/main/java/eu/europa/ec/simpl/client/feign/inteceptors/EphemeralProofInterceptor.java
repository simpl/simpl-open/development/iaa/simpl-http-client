package eu.europa.ec.simpl.client.feign.inteceptors;

import eu.europa.ec.simpl.client.core.EphemeralProofFlow;
import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import eu.europa.ec.simpl.client.feign.annotations.PreflightEphemeralProof;
import eu.europa.ec.simpl.client.feign.exchanges.AuthorityEphemeralProofExchange;
import eu.europa.ec.simpl.client.feign.exchanges.ParticipantEphemeralProofExchange;
import feign.Feign;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class EphemeralProofInterceptor extends EphemeralProofFlow<Feign.Builder, RequestTemplate>
        implements RequestInterceptor {

    public EphemeralProofInterceptor(
            EphemeralProofAdapter ephemeralProofAdapter,
            AuthorityUrlSupplier authorityUrlSupplier,
            Feign.Builder client) {
        super(ephemeralProofAdapter, authorityUrlSupplier, client);
    }

    @Override
    protected boolean shouldDoPreflight(RequestTemplate request) {
        String methodName = request.methodMetadata().method().getName();
        return Stream.of(request.methodMetadata().method().getDeclaringClass().getDeclaredMethods())
                .filter(method -> Objects.equals(method.getName(), methodName))
                .filter(method -> method.isAnnotationPresent(PreflightEphemeralProof.class))
                .findFirst()
                .flatMap(method -> Optional.ofNullable(method.getAnnotation(PreflightEphemeralProof.class)))
                .map(PreflightEphemeralProof::value)
                .orElse(Boolean.FALSE);
    }

    @Override
    protected void sendEphemeralProofToPeer(String participantUrl, String ephemeralProof) {
        client.target(ParticipantEphemeralProofExchange.class, participantUrl).sendEphemeralProof(ephemeralProof);
    }

    @Override
    protected String getEphemeralProofFromAuthority() {
        return client.target(AuthorityEphemeralProofExchange.class, authorityUrlSupplier.getAuthorityUrl())
                .token();
    }

    @Override
    protected URI getRequestUrl(RequestTemplate request) throws URISyntaxException {
        return new URI(request.feignTarget().url());
    }

    @Override
    public void apply(RequestTemplate template) {
        execute(template);
    }
}
