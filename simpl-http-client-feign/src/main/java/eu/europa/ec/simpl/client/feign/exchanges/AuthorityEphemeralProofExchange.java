package eu.europa.ec.simpl.client.feign.exchanges;

import feign.RequestLine;

public interface AuthorityEphemeralProofExchange extends ParticipantEphemeralProofExchange {
    @RequestLine("POST /identity-api/mtls/token")
    String token();
}
