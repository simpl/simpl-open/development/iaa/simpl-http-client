package eu.europa.ec.simpl.client.feign;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import feign.FeignException;
import feign.Response;
import feign.codec.Decoder;
import feign.jackson.JacksonDecoder;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class CustomDecoder implements Decoder {
    private static final String CONTENT_TYPE_HEADER = "content-type";
    private static final String APPLICATION_JSON_VALUE = "application/json";

    private final Decoder jacksonDecoder = new JacksonDecoder(List.of(new JavaTimeModule()));
    private final Decoder defaultDecoder = new Default();

    @Override
    public Object decode(Response response, Type type) throws IOException, FeignException {
        if (response.headers().entrySet().stream()
                .filter(e -> e.getKey().equalsIgnoreCase(CONTENT_TYPE_HEADER))
                .flatMap(e -> e.getValue().stream())
                .map(String::toLowerCase)
                .anyMatch(h -> h.startsWith(APPLICATION_JSON_VALUE))) {
            return jacksonDecoder.decode(response, type);
        } else {
            return defaultDecoder.decode(response, type);
        }
    }
}
