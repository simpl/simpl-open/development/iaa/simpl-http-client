package eu.europa.ec.simpl.client.feign;

import dagger.Component;
import javax.inject.Singleton;

@Singleton
@Component
public interface FeignSimplClientFactory {
    FeignSimplClient get();
}
