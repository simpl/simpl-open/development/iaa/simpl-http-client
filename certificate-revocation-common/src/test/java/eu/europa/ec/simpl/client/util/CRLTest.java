package eu.europa.ec.simpl.client.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CRLTest {
    @Mock
    private HTTP http;

    @Mock
    private X509CRL x509CRL;

    @InjectMocks
    private CRL crl;

    @Test
    void testWithoutUrl() throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-ocsp.p12").getCertificate("1");
        assertThatThrownBy(() -> crl.isRevoked(certificate))
                .isInstanceOf(CertificateException.class)
                .hasMessage("CRL List not available");
    }

    @Test
    void testValid() throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-crl.p12").getCertificate("1");
        when(http.downloadCRL("http://test.com/crl")).thenReturn(x509CRL);
        assertThat(crl.isRevoked(certificate)).isFalse();
    }

    @Test
    void testRevoked() throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-crl.p12").getCertificate("1");
        when(http.downloadCRL("http://test.com/crl")).thenReturn(x509CRL);
        when(x509CRL.isRevoked(certificate)).thenReturn(true);
        assertThat(crl.isRevoked(certificate)).isTrue();
    }
}
