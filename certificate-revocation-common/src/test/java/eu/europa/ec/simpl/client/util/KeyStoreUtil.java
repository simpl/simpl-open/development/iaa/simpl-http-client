package eu.europa.ec.simpl.client.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class KeyStoreUtil {
    public static KeyStore getKeyStore(String path)
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
        var keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(Files.newInputStream(getPath(path)), "password".toCharArray());
        return keyStore;
    }

    private static Path getPath(String path) {
        try {
            return Path.of(CRLTest.class.getClassLoader().getResource(path).toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
