package eu.europa.ec.simpl.client.util;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.operator.OperatorCreationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CertificateRevocationTest {

    @Mock
    private OCSP ocsp;

    @Mock
    private CRL crl;

    @Mock
    private X509Certificate certificate;

    @InjectMocks
    private CertificateRevocation certificateRevocation;

    @Test
    void ocspValid() {
        assertThatCode(() -> certificateRevocation.verify(certificate)).doesNotThrowAnyException();
    }

    @Test
    void ocspRevoked()
            throws OCSPException, CertificateException, IOException, OperatorCreationException, InterruptedException {
        when(ocsp.isRevoked(certificate)).thenReturn(true);
        assertThatThrownBy(() -> certificateRevocation.verify(certificate))
                .isInstanceOf(CertificateException.class)
                .hasMessage("OCSP certificate revoked");
    }

    @Test
    void crlValid()
            throws OCSPException, CertificateException, IOException, OperatorCreationException, InterruptedException {
        when(ocsp.isRevoked(certificate)).thenThrow(new RuntimeException("test"));
        assertThatCode(() -> certificateRevocation.verify(certificate)).doesNotThrowAnyException();
    }

    @Test
    void crlRevoked()
            throws OCSPException, CertificateException, IOException, OperatorCreationException, InterruptedException {
        when(ocsp.isRevoked(certificate)).thenThrow(new RuntimeException("test"));
        when(crl.isRevoked(certificate)).thenReturn(true);
        assertThatThrownBy(() -> certificateRevocation.verify(certificate))
                .isInstanceOf(CertificateException.class)
                .hasMessage("CRL certificate revoked");
    }
}
