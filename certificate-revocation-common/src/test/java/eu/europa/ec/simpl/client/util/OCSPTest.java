package eu.europa.ec.simpl.client.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.util.Date;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.ocsp.*;
import org.bouncycastle.operator.OperatorCreationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OCSPTest {
    @Mock
    private HTTP http;

    @Mock
    private OCSPResp ocspResp;

    @Mock
    private BasicOCSPResp basicOCSPResp;

    @Mock
    private Extension extension;

    @Mock
    private SingleResp singleResp;

    @Mock
    private CertificateID certificateID;

    @InjectMocks
    private OCSP ocsp;

    @Test
    void testWithoutUrl() throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-crl.p12").getCertificate("1");
        assertThatThrownBy(() -> ocsp.isRevoked(certificate))
                .isInstanceOf(CertificateException.class)
                .hasMessage("Authority information access is not present");
    }

    @Test
    void testWithoutAuthority() throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-ocsp.p12").getCertificate("1");
        assertThatThrownBy(() -> ocsp.isRevoked(certificate))
                .isInstanceOf(CertificateException.class)
                .hasMessage("CA certificate not found");
    }

    @Test
    void testOCSPResponseError()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, InterruptedException,
                    OCSPException, OperatorCreationException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-ocsp.p12").getCertificate("1");
        when(http.retrieveCACertificate(eq("http://test.com/ca"), anyString())).thenReturn(certificate);
        when(http.ocspResponse(eq("http://test.com/ocsp"), any(OCSPReq.class))).thenReturn(ocspResp);
        when(ocspResp.getStatus()).thenReturn(OCSPResp.INTERNAL_ERROR);
        assertThat(ocsp.isRevoked(certificate)).isTrue();
    }

    @Test
    void testOCSPResponseValidateNonceError()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, InterruptedException,
                    OCSPException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-ocsp.p12").getCertificate("1");
        when(http.retrieveCACertificate(eq("http://test.com/ca"), anyString())).thenReturn(certificate);
        when(http.ocspResponse(eq("http://test.com/ocsp"), any(OCSPReq.class))).thenReturn(ocspResp);
        when(ocspResp.getStatus()).thenReturn(OCSPResp.SUCCESSFUL);
        when(ocspResp.getResponseObject()).thenReturn(basicOCSPResp);
        when(basicOCSPResp.getExtension(any())).thenReturn(extension);
        when(extension.getEncoded()).thenReturn(new byte[0]);
        assertThatThrownBy(() -> ocsp.isRevoked(certificate))
                .isInstanceOf(CertificateException.class)
                .hasMessage("Invalid nonce in response");
    }

    @Test
    void testOCSPResponseValidateCertificateIdError()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, InterruptedException,
                    OCSPException, OperatorCreationException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-ocsp.p12").getCertificate("1");
        when(http.retrieveCACertificate(eq("http://test.com/ca"), anyString())).thenReturn(certificate);
        when(http.ocspResponse(eq("http://test.com/ocsp"), any(OCSPReq.class)))
                .thenAnswer(invocationOnMock -> ocspResp(invocationOnMock, false, false));
        assertThat(ocsp.isRevoked(certificate)).isTrue();
    }

    @Test
    void testOCSPResponseCertStatusRevoked()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, InterruptedException,
                    OCSPException, OperatorCreationException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-ocsp.p12").getCertificate("1");
        when(http.retrieveCACertificate(eq("http://test.com/ca"), anyString())).thenReturn(certificate);
        when(http.ocspResponse(eq("http://test.com/ocsp"), any(OCSPReq.class)))
                .thenAnswer(invocationOnMock -> ocspResp(invocationOnMock, true, true));
        assertThat(ocsp.isRevoked(certificate)).isTrue();
    }

    @Test
    void testOCSPValid()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, InterruptedException,
                    OCSPException, OperatorCreationException {
        var certificate =
                (X509Certificate) KeyStoreUtil.getKeyStore("cert-ocsp.p12").getCertificate("1");
        when(http.retrieveCACertificate(eq("http://test.com/ca"), anyString())).thenReturn(certificate);
        when(http.ocspResponse(eq("http://test.com/ocsp"), any(OCSPReq.class)))
                .thenAnswer(invocationOnMock -> ocspResp(invocationOnMock, true, false));
        assertThat(ocsp.isRevoked(certificate)).isFalse();
    }

    private Object ocspResp(InvocationOnMock invocationOnMock, boolean validCertificateId, boolean revoked) {
        var request = invocationOnMock.getArgument(1, OCSPReq.class);
        when(ocspResp.getStatus()).thenReturn(OCSPResp.SUCCESSFUL);
        try {
            when(ocspResp.getResponseObject()).thenReturn(basicOCSPResp);
            when(basicOCSPResp.getExtension(any())).thenReturn(extension);
            when(basicOCSPResp.getResponses()).thenReturn(new SingleResp[] {singleResp});
            when(extension.getEncoded())
                    .thenReturn(request.getExtension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce)
                            .getEncoded());
            when(singleResp.getCertID()).thenReturn(certificateID);
            if (validCertificateId) {
                when(certificateID.getSerialNumber()).thenReturn(BigInteger.ONE);
                when(certificateID.getIssuerNameHash()).thenReturn(new byte[] {
                    -50, -23, 106, 107, -59, -80, 46, -37, -18, 18, -59, 91, 30, 112, 39, 84, -91, 70, 21, -108, 43, 78,
                    101, -36, 22, -109, -117, 73, -66, 121, -115, 47
                });
            }
            if (revoked) {
                when(singleResp.getCertStatus()).thenReturn(new RevokedStatus(Date.from(Instant.now())));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ocspResp;
    }
}
