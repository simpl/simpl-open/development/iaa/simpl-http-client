package eu.europa.ec.simpl.client.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class HTTPTest {

    @Mock
    private HttpClient httpClient;

    @Mock
    private HttpResponse<Object> httpResponse;

    private HTTP http;

    @BeforeEach
    public void init() {
        http = new HTTP();
        http.httpClient = httpClient;
    }

    @Test
    public void downloadCRLTest_whenValidCrlDer_returnNotNull() throws IOException, InterruptedException {
        var crlURL = "http://junit-url";
        var body = getClass().getResourceAsStream("crl.der");
        when(httpClient.send(any(), any())).thenReturn(httpResponse);
        when(httpResponse.body()).thenReturn(body);
        var response = http.downloadCRL(crlURL);
        assertThat(response).isNotNull();
    }

    @Test
    public void downloadCRLTest_whenInvvalidCrlDer_returnNull() throws IOException, InterruptedException {
        var crlURL = "http://junit-url";
        var body = new ByteArrayInputStream("invalibody".getBytes());
        when(httpClient.send(any(), any())).thenReturn(httpResponse);
        when(httpResponse.body()).thenReturn(body);
        var response = http.downloadCRL(crlURL);
        assertThat(response).isNull();
    }

    @Test
    public void retrieveCACertificateTest_whenValidServerDer_returnNotNull() throws IOException, InterruptedException {
        var crlURL = "http://junit-url";
        var body = getClass().getResourceAsStream("server.der");
        when(httpClient.send(any(), any())).thenReturn(httpResponse);
        when(httpResponse.body()).thenReturn(body);
        var response = http.retrieveCACertificate(crlURL, "O=Internet Widgits Pty Ltd,ST=Some-State,C=AU");
        assertThat(response).isNotNull();
    }

    @Test
    public void retrieveCACertificateTest_whenInvalidServerDer_returnNull() throws IOException, InterruptedException {
        var crlURL = "http://junit-url";
        var body = new ByteArrayInputStream("invalibody".getBytes());
        when(httpClient.send(any(), any())).thenReturn(httpResponse);
        when(httpResponse.body()).thenReturn(body);
        var response = http.retrieveCACertificate(crlURL, "O=Internet Widgits Pty Ltd,ST=Some-State,C=AU");
        assertThat(response).isNull();
    }
}
