package eu.europa.ec.simpl.client.util;

import dagger.Component;
import javax.inject.Singleton;

@Singleton
@Component
public interface CertificateRevocationFactory {
    CertificateRevocation get();
}
