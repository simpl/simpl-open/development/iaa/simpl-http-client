package eu.europa.ec.simpl.client.util;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.bouncycastle.asn1.ASN1IA5String;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;

@Singleton
class CRL {
    private final HTTP http;

    @Inject
    CRL(HTTP http) {
        this.http = http;
    }

    boolean isRevoked(X509Certificate cert) throws CertificateException {
        var crlDistPoints = getCrlDistributionPoints(cert);
        if (crlDistPoints.isEmpty()) {
            throw new CertificateException("CRL List not available");
        }
        return crlDistPoints.stream()
                .map(http::downloadCRL)
                .filter(Objects::nonNull)
                .anyMatch(crl -> crl.isRevoked(cert));
    }

    private List<String> getCrlDistributionPoints(X509Certificate cert) throws CertificateException {
        var distPoint = CRLDistPoint.fromExtensions(new JcaX509CertificateHolder(cert).getExtensions());
        return Optional.ofNullable(distPoint).map(CRLDistPoint::getDistributionPoints).stream()
                .flatMap(Arrays::stream)
                .map(DistributionPoint::getDistributionPoint)
                .filter(Objects::nonNull)
                .filter(dpn -> dpn.getType() == DistributionPointName.FULL_NAME)
                .map(dpn -> GeneralNames.getInstance(dpn.getName()).getNames())
                .flatMap(Arrays::stream)
                .filter(genName -> genName.getTagNo() == GeneralName.uniformResourceIdentifier)
                .map(genName -> ASN1IA5String.getInstance(genName.getName()).toString())
                .toList();
    }
}
