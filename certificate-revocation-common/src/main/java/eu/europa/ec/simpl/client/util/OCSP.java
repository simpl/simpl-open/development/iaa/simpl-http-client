package eu.europa.ec.simpl.client.util;

import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Objects;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.ocsp.*;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
class OCSP {
    private static final Logger logger = LoggerFactory.getLogger(OCSP.class);

    private final HTTP http;

    @Inject
    OCSP(HTTP http) {
        this.http = http;
    }

    boolean isRevoked(X509Certificate userCert)
            throws CertificateException, OperatorCreationException, OCSPException, IOException, InterruptedException {
        var caCert = retrieveCACertificate(userCert);
        var request = generateOCSPRequest(caCert, userCert.getSerialNumber());
        var ocspUrl = extractOCSPUrl(userCert);
        logger.debug("OCSP url: {}", ocspUrl);
        var ocspResponse = http.ocspResponse(ocspUrl, request);
        if (ocspResponse.getStatus() == OCSPResp.SUCCESSFUL) {
            var responseObject = (BasicOCSPResp) ocspResponse.getResponseObject();
            validateNonce(request, responseObject);
            return Arrays.stream(responseObject.getResponses())
                    .findFirst()
                    .filter(resp -> validateCertificateId(caCert, userCert, resp.getCertID()))
                    .map(resp -> resp.getCertStatus() != CertificateStatus.GOOD)
                    .orElse(true);
        } else {
            logger.error("OCSP Status {}", ocspResponse.getStatus());
            return true;
        }
    }

    private X509Certificate retrieveCACertificate(X509Certificate certificate) throws CertificateException {
        var issuer = certificate.getIssuerX500Principal().getName();
        var authorityInformationAccess = extractAuthorityInformationAccess(certificate);
        var descriptions = authorityInformationAccess.getAccessDescriptions();
        return Arrays.stream(descriptions)
                .filter(ad -> ad.getAccessMethod().equals(X509ObjectIdentifiers.id_ad_caIssuers))
                .map(AccessDescription::getAccessLocation)
                .filter(l -> l.getTagNo() == GeneralName.uniformResourceIdentifier)
                .map(l -> http.retrieveCACertificate(l.getName().toString(), issuer))
                .filter(Objects::nonNull)
                .findAny()
                .orElseThrow(() -> new CertificateException("CA certificate not found"));
    }

    private AuthorityInformationAccess extractAuthorityInformationAccess(X509Certificate certificate)
            throws CertificateException {
        var wrapper = new JcaX509CertificateHolder(certificate);
        var authorityInformationAccess = AuthorityInformationAccess.fromExtensions(wrapper.getExtensions());
        if (authorityInformationAccess == null) {
            throw new CertificateException("Authority information access is not present");
        }
        return authorityInformationAccess;
    }

    private OCSPReq generateOCSPRequest(X509Certificate issuerCert, BigInteger serialNumber)
            throws CertificateEncodingException, OperatorCreationException, OCSPException {
        var id = getCertificateID(issuerCert, serialNumber);
        var nonce = BigInteger.valueOf(System.currentTimeMillis());
        var nonceExtension =
                new Extension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce, true, new DEROctetString(nonce.toByteArray()));
        var gen = new OCSPReqBuilder();
        gen.addRequest(id);
        gen.setRequestExtensions(new Extensions(nonceExtension));
        return gen.build();
    }

    private String extractOCSPUrl(X509Certificate cert) throws CertificateException {
        var aiaExtension =
                AuthorityInformationAccess.fromExtensions(new JcaX509CertificateHolder(cert).getExtensions());
        return Arrays.stream(aiaExtension.getAccessDescriptions())
                .filter(ad -> ad.getAccessMethod().equals(X509ObjectIdentifiers.id_ad_ocsp))
                .map(ad -> ad.getAccessLocation().getName().toASN1Primitive().toString())
                .findFirst()
                .orElseThrow(() -> new CertificateException("No OCSP url found"));
    }

    private void validateNonce(OCSPReq request, BasicOCSPResp basicOcspResp) throws IOException, CertificateException {
        var reqNonce =
                request.getExtension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce).getEncoded();
        var respNonce = basicOcspResp
                .getExtension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce)
                .getEncoded();
        if (!Arrays.equals(reqNonce, respNonce)) {
            throw new CertificateException("Invalid nonce in response");
        }
    }

    private boolean validateCertificateId(
            X509Certificate issuerCert, X509Certificate userCert, CertificateID certificateId) {
        try {
            var expectedId = getCertificateID(issuerCert, userCert.getSerialNumber());
            return expectedId.getSerialNumber().equals(certificateId.getSerialNumber())
                    && Arrays.equals(expectedId.getIssuerNameHash(), certificateId.getIssuerNameHash());
        } catch (Exception e) {
            logger.error("Error validating certificate id", e);
            return false;
        }
    }

    private CertificateID getCertificateID(X509Certificate issuerCert, BigInteger serialNumber)
            throws OperatorCreationException, OCSPException, CertificateEncodingException {
        var jcaDigestCalculatorProviderBuilder = new JcaDigestCalculatorProviderBuilder();
        var digestCalculator = jcaDigestCalculatorProviderBuilder.build();
        var algorithmIdentifier = new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha256, DERNull.INSTANCE);
        var calculatorValue = digestCalculator.get(algorithmIdentifier);
        return new CertificateID(calculatorValue, new JcaX509CertificateHolder(issuerCert), serialNumber);
    }
}
