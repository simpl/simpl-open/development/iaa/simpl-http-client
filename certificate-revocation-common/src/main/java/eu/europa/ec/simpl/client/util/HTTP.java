package eu.europa.ec.simpl.client.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.bouncycastle.cert.ocsp.OCSPReq;
import org.bouncycastle.cert.ocsp.OCSPResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
class HTTP {
    private static final Logger logger = LoggerFactory.getLogger(HTTP.class);

    protected HttpClient httpClient = HttpClient.newHttpClient();

    @Inject
    HTTP() {}

    X509CRL downloadCRL(String crlURL) {
        logger.debug("Downloading CRL from {}", crlURL);
        try (var response = inputStream(crlURL)) {
            var cf = CertificateFactory.getInstance("X.509");
            return (X509CRL) cf.generateCRL(response);
        } catch (Exception e) {
            logger.error("Error retrieving crl from {}", crlURL, e);
            return null;
        }
    }

    X509Certificate retrieveCACertificate(String issuerUrl, String issuer) {
        logger.debug("Downloading certificate from {} with issuer {}", issuerUrl, issuer);
        try (var response = inputStream(issuerUrl)) {
            var certificateFactory = CertificateFactory.getInstance("X.509");
            return certificateFactory.generateCertificates(response).stream()
                    .filter(X509Certificate.class::isInstance)
                    .map(X509Certificate.class::cast)
                    .filter(c -> c.getSubjectX500Principal().getName().equals(issuer))
                    .findAny()
                    .orElse(null);
        } catch (Exception e) {
            logger.error("Error retrieving certificate from {} with issuer {}", issuerUrl, issuer, e);
            return null;
        }
    }

    OCSPResp ocspResponse(String ocspUrl, OCSPReq request) throws IOException, InterruptedException {
        var response = httpClient
                .send(
                        HttpRequest.newBuilder(URI.create(ocspUrl))
                                .POST(HttpRequest.BodyPublishers.ofByteArray(request.getEncoded()))
                                .headers(
                                        "Content-Type",
                                        "application/ocsp-request",
                                        "Accept",
                                        "application/ocsp-response")
                                .build(),
                        HttpResponse.BodyHandlers.ofByteArray())
                .body();
        return new OCSPResp(response);
    }

    private InputStream inputStream(String url) throws IOException, InterruptedException {
        return httpClient
                .send(HttpRequest.newBuilder(URI.create(url)).GET().build(), HttpResponse.BodyHandlers.ofInputStream())
                .body();
    }
}
