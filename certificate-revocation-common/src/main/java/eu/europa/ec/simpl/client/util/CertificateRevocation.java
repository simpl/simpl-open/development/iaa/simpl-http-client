package eu.europa.ec.simpl.client.util;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.operator.OperatorCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class CertificateRevocation {
    private static final Logger logger = LoggerFactory.getLogger(CertificateRevocation.class);

    private final OCSP ocsp;
    private final CRL crl;

    @Inject
    CertificateRevocation(OCSP ocsp, CRL crl) {
        this.ocsp = ocsp;
        this.crl = crl;
    }

    public void verify(X509Certificate certificate) throws CertificateException {
        try {
            if (ocsp.isRevoked(certificate)) {
                throw new CertificateException("OCSP certificate revoked");
            }
        } catch (OCSPException | IOException | OperatorCreationException | RuntimeException e) {
            checkOcspFallback(certificate, e);
        } catch (InterruptedException e) {
            checkOcspFallback(certificate, e);
            Thread.currentThread().interrupt();
        }
    }

    private void checkOcspFallback(X509Certificate certificate, Exception e) throws CertificateException {
        logger.error("Error on OCSP, falling back on CRL", e);
        if (crl.isRevoked(certificate)) {
            throw new CertificateException("CRL certificate revoked", e);
        }
    }
}
