package eu.europa.ec.simpl.client.okhttp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.ssl.SslConfig;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import eu.europa.ec.simpl.client.core.suppliers.SslConfigSupplier;
import java.io.IOException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class OkHttpClientConfiguratorTest {

    @Mock
    private OkHttpClient fallbackClient;

    @Mock
    private OkHttpClient.Builder client;

    private OkHttpClientConfigurator okHttpClientConfigurator;

    @BeforeEach
    public void init() {
        when(client.build()).thenReturn(fallbackClient);
        okHttpClientConfigurator = new OkHttpClientConfigurator(client);
    }

    @Test
    public void configureSslTest() {
        var sslConfigSupplier = mock(SslConfigSupplier.class);
        var sslConfig = mock(SslConfig.class);
        var socketFactory = mock(SSLSocketFactory.class);
        var x509TrustManager = mock(X509TrustManager.class);
        var sslContext = mock(SSLContext.class);
        var hostnameVerifier = mock(HostnameVerifier.class);
        when(sslConfig.getSslContext()).thenReturn(sslContext);
        when(sslConfigSupplier.getSslConfig()).thenReturn(sslConfig);
        when(sslContext.getSocketFactory()).thenReturn(socketFactory);
        when(sslConfig.getTrustManager()).thenReturn(x509TrustManager);
        when(client.sslSocketFactory(socketFactory, x509TrustManager)).thenReturn(client);
        when(sslConfig.getHostnameVerifier()).thenReturn(hostnameVerifier);
        var result = okHttpClientConfigurator.configureSsl(sslConfigSupplier);
        assertThat(result).isEqualTo(okHttpClientConfigurator);
    }

    @Test
    public void configureTierOneTokenPropagatorTest() {
        var authorizationHeaderSupplier = mock(AuthorizationHeaderSupplier.class);
        var result = okHttpClientConfigurator.configureTierOneTokenPropagator(authorizationHeaderSupplier);
        verify(client).addInterceptor(notNull(TierOneTokenPropagator.class));
        assertThat(result).isEqualTo(okHttpClientConfigurator);
    }

    @Test
    public void configureEphemeralProofPreflightTest() {
        var authorityUrlSupplier = mock(AuthorityUrlSupplier.class);
        var ephemeralProofAdapter = mock(EphemeralProofAdapter.class);
        var result =
                okHttpClientConfigurator.configureEphemeralProofPreflight(authorityUrlSupplier, ephemeralProofAdapter);
        verify(client).addInterceptor(notNull(TierOneTokenPropagator.class));
        assertThat(result).isEqualTo(okHttpClientConfigurator);
    }

    @Test
    public void configureMTLSFallbackTest() {
        var result = okHttpClientConfigurator.configureMTLSFallback();
        verify(client, times(1)).addInterceptor(any());
        assertThat(result).isEqualTo(okHttpClientConfigurator);
    }

    @Test
    public void configureEncoderTest() {
        var result = okHttpClientConfigurator.configureEncoder();
        assertThat(result).isEqualTo(okHttpClientConfigurator);
    }

    @Test
    public void configureDecoderTest() {
        var result = okHttpClientConfigurator.configureDecoder();
        assertThat(result).isEqualTo(okHttpClientConfigurator);
    }

    @Test
    public void intercept_whenSuccessChain_success() throws IOException {
        var argc = ArgumentCaptor.forClass(Interceptor.class);
        okHttpClientConfigurator.configureMTLSFallback();
        verify(client).addInterceptor(argc.capture());
        var interceptor = argc.getValue();
        var chain = mock(Chain.class);
        var request = mock(Request.class);
        var response = mock(Response.class);
        when(chain.request()).thenReturn(request);
        when(chain.proceed(request)).thenReturn(response);
        var result = interceptor.intercept(chain);
        assertThat(result).isEqualTo(response);
    }

    @Test
    public void intercept_whenSSLHandshakeException_fallbackClientNewCall() throws IOException {
        var argc = ArgumentCaptor.forClass(Interceptor.class);
        okHttpClientConfigurator.configureMTLSFallback();
        verify(client).addInterceptor(argc.capture());
        var interceptor = argc.getValue();
        var chain = mock(Chain.class);
        var request = mock(Request.class);
        var call = mock(Call.class);
        var response = mock(Response.class);
        when(chain.proceed(any())).thenThrow(SSLHandshakeException.class);
        when(chain.request()).thenReturn(request);
        when(fallbackClient.newCall(request)).thenReturn(call);
        when(call.execute()).thenReturn(response);
        var result = interceptor.intercept(chain);
        assertThat(result).isEqualTo(response);
    }
}
