package eu.europa.ec.simpl.client.okhttp;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.client.core.SimplClient;
import eu.europa.ec.simpl.client.core.SimplClient.Builder;
import okhttp3.OkHttpClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class OkHttpSimplClientTest {
    @Mock
    private SimplClient simplClient;

    private OkHttpSimplClient okHttpSimplClient;

    @BeforeEach
    public void init() {
        okHttpSimplClient = new OkHttpSimplClient(simplClient);
    }

    @Test
    public void builderTestBuilder() {
        OkHttpClient.Builder builder = mock(OkHttpClient.Builder.class);
        okHttpSimplClient.builder(builder);
        verify(simplClient).builder(notNull());
    }

    @Test
    public void builderTest() {
        assertDoesNotThrow(() -> okHttpSimplClient.builder());
    }

    @Test
    public void buildTest() {
        Builder<Object> simplBuilder = mock();
        when(simplClient.builder(any())).thenReturn(simplBuilder);
        assertDoesNotThrow(() -> okHttpSimplClient.build());
    }
}
