package eu.europa.ec.simpl.client.okhttp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import java.io.EOFException;
import java.io.IOException;
import java.util.stream.Stream;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class EphemeralProofInterceptorTest {
    @Mock
    private EphemeralProofAdapter ephemeralProofAdapter;

    @Mock
    private AuthorityUrlSupplier authorityUrlSupplier;

    @Mock
    private OkHttpClient.Builder client;

    private EphemeralProofInterceptor ephemeralProofInterceptor;

    @BeforeEach
    public void init() {
        ephemeralProofInterceptor = new EphemeralProofInterceptor(ephemeralProofAdapter, authorityUrlSupplier, client);
    }

    public static Stream<Arguments> shouldDoPreflight_whenNoPreflightPath_thenReturnFalse() {
        return Stream.of(
                Arguments.of("http://host/identity-api/mtls/token"),
                Arguments.of("http://host/identity-api/mtls/public-key"),
                Arguments.of("http://host/user-api/mtls/ephemeral-proof"));
    }

    @ParameterizedTest
    @MethodSource
    public void shouldDoPreflight_whenNoPreflightPath_thenReturnFalse(String path) {
        var request = new Request.Builder().url(path).build();
        var result = ephemeralProofInterceptor.shouldDoPreflight(request);
        assertThat(result).isFalse();
    }

    @Test
    public void shouldDoPreflight_whenPreflightPath_thenReturnTrue() {
        var request = new Request.Builder().url("http://host/path").build();
        var result = ephemeralProofInterceptor.shouldDoPreflight(request);
        assertThat(result).isTrue();
    }

    @Test
    public void sendEphemeralProofToPeerTest() throws IOException {
        var participantUrl = "http://junit/path";
        var ephemeralProof = "ephemeralProofJunit";
        var okHttpClient = mock(OkHttpClient.class);
        var call = mock(Call.class);
        var response = mock(Response.class);
        when(client.build()).thenReturn(okHttpClient);
        when(okHttpClient.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.code()).thenReturn(200);
        ephemeralProofInterceptor.sendEphemeralProofToPeer(participantUrl, ephemeralProof);
    }

    @Test
    public void getEphemeralProofFromAuthorityTest() throws IOException {
        var okHttpClient = mock(OkHttpClient.class);
        var call = mock(Call.class);
        var response = mock(Response.class);
        var body = mock(ResponseBody.class);
        when(client.build()).thenReturn(okHttpClient);
        when(okHttpClient.newCall(any())).thenReturn(call);
        when(call.execute()).thenReturn(response);
        when(response.body()).thenReturn(body);
        when(body.string()).thenReturn("Response body");
        when(authorityUrlSupplier.getAuthorityUrl()).thenReturn("http://junit");
        var result = ephemeralProofInterceptor.getEphemeralProofFromAuthority();
        assertThat(result).isEqualTo("Response body");
    }

    @Test
    public void getEphemeralProofFromAuthority_whenThrowIOException_thenThrowException() throws IOException {
        var okHttpClient = mock(OkHttpClient.class);
        var call = mock(Call.class);
        var ex = mock(IOException.class);
        var exEOFException = mock(EOFException.class);
        when(ex.getCause()).thenReturn(exEOFException);
        when(authorityUrlSupplier.getAuthorityUrl()).thenReturn("http://junit");
        when(client.build()).thenReturn(okHttpClient);
        when(okHttpClient.newCall(any())).thenReturn(call);
        when(call.execute()).thenThrow(ex);
        assertThrows(RuntimeWrapperException.class, () -> ephemeralProofInterceptor.getEphemeralProofFromAuthority());
    }
}
