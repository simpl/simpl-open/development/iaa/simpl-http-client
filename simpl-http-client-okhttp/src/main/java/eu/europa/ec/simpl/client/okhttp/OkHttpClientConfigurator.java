package eu.europa.ec.simpl.client.okhttp;

import eu.europa.ec.simpl.client.core.AbstractClientConfigurator;
import eu.europa.ec.simpl.client.core.ClientConfigurator;
import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.suppliers.AuthenticationProviderConfigSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import eu.europa.ec.simpl.client.core.suppliers.SslConfigSupplier;
import java.io.IOException;
import javax.net.ssl.SSLHandshakeException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OkHttpClientConfigurator extends AbstractClientConfigurator<OkHttpClient.Builder> {

    private final OkHttpClient fallbackClient;

    public OkHttpClientConfigurator(OkHttpClient.Builder client) {
        super(client);
        fallbackClient = client.build();
    }

    @Override
    public AuthenticationProviderAdapter buildAuthenticationProviderAdapter(
            AuthenticationProviderConfigSupplier authenticationProviderConfigSupplier) {
        return new OkHttpAuthenticationProviderAdapter(
                fallbackClient, authenticationProviderConfigSupplier.getAuthenticationProviderConfig());
    }

    @Override
    public ClientConfigurator<OkHttpClient.Builder> configureSsl(SslConfigSupplier sslConfigSupplier) {
        var config = sslConfigSupplier.getSslConfig();
        getClient()
                .sslSocketFactory(
                        config.getSslContext().getSocketFactory(),
                        sslConfigSupplier.getSslConfig().getTrustManager())
                .hostnameVerifier(sslConfigSupplier.getSslConfig().getHostnameVerifier());
        return this;
    }

    @Override
    public ClientConfigurator<OkHttpClient.Builder> configureTierOneTokenPropagator(
            @Nullable AuthorizationHeaderSupplier authorizationHeaderSupplier) {
        if (authorizationHeaderSupplier != null) {
            getClient().addInterceptor(new TierOneTokenPropagator(authorizationHeaderSupplier));
        }
        return this;
    }

    @Override
    public ClientConfigurator<OkHttpClient.Builder> configureEphemeralProofPreflight(
            AuthorityUrlSupplier authorityUrlSupplier, EphemeralProofAdapter ephemeralProofAdapter) {
        if (ephemeralProofAdapter != null) {
            getClient()
                    .addInterceptor(
                            new EphemeralProofInterceptor(ephemeralProofAdapter, authorityUrlSupplier, getClient()));
        }
        return this;
    }

    @Override
    public ClientConfigurator<OkHttpClient.Builder> configureMTLSFallback() {
        getClient().addInterceptor(new MTLSFallbackInterceptor(fallbackClient));
        return this;
    }

    @Override
    public ClientConfigurator<OkHttpClient.Builder> configureEncoder() {
        return this;
    }

    @Override
    public ClientConfigurator<OkHttpClient.Builder> configureDecoder() {
        return this;
    }

    private static class MTLSFallbackInterceptor implements Interceptor {

        private static final Logger log = LoggerFactory.getLogger(MTLSFallbackInterceptor.class);

        private final OkHttpClient fallbackClient;

        private MTLSFallbackInterceptor(OkHttpClient fallbackClient) {
            this.fallbackClient = fallbackClient;
        }

        @NotNull @Override
        public Response intercept(@NotNull Chain chain) throws IOException {
            try {
                return chain.proceed(chain.request());
            } catch (SSLHandshakeException e) {
                log.error(
                        "Exception while attempting mTLS request {}, falling back to backup client",
                        "%s %s"
                                .formatted(
                                        chain.request().method(),
                                        chain.request().url()),
                        e);
                return fallbackClient.newCall(chain.request()).execute();
            }
        }
    }
}
