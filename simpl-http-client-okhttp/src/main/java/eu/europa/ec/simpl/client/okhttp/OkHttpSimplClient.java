package eu.europa.ec.simpl.client.okhttp;

import eu.europa.ec.simpl.client.core.SimplClient;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Singleton;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

@Singleton
public class OkHttpSimplClient {

    private final SimplClient simplClient;

    @Inject
    public OkHttpSimplClient(SimplClient simplClient) {
        this.simplClient = simplClient;
    }

    public SimplClient.Builder<OkHttpClient.Builder> builder(OkHttpClient.Builder builder) {
        return simplClient.builder(new OkHttpClientConfigurator(builder));
    }

    public SimplClient.Builder<OkHttpClient.Builder> builder() {
        return builder(createDefaultOkHttpClientBuilder());
    }

    public OkHttpClient.Builder build() {
        return builder().build();
    }

    private static OkHttpClient.Builder createDefaultOkHttpClientBuilder() {
        return new OkHttpClient.Builder()
                .connectTimeout(BigDecimal.TEN.intValue(), TimeUnit.SECONDS)
                .readTimeout(BigDecimal.TEN.intValue(), TimeUnit.SECONDS)
                .writeTimeout(BigDecimal.TEN.intValue(), TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(chain -> chain.proceed(chain.request()
                        .newBuilder()
                        .header("Connection", "close")
                        .build()))
                .connectionPool(
                        new ConnectionPool(BigDecimal.ZERO.intValue(), BigDecimal.TEN.intValue(), TimeUnit.MINUTES));
    }
}
