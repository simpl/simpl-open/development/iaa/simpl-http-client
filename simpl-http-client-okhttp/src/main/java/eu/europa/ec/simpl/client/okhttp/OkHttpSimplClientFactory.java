package eu.europa.ec.simpl.client.okhttp;

import dagger.Component;
import javax.inject.Singleton;

@Singleton
@Component
public interface OkHttpSimplClientFactory {
    OkHttpSimplClient get();
}
