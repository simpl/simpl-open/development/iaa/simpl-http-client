package eu.europa.ec.simpl.client.okhttp;

import eu.europa.ec.simpl.client.core.AbstractTierOneTokenPropagator;
import eu.europa.ec.simpl.client.core.suppliers.AuthorizationHeaderSupplier;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

public class TierOneTokenPropagator extends AbstractTierOneTokenPropagator implements Interceptor {

    protected TierOneTokenPropagator(AuthorizationHeaderSupplier authorizationHeaderSupplier) {
        super(authorizationHeaderSupplier);
    }

    @NotNull @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        var authorization = getAuthorizationHeader();
        var request = chain.request();
        if (authorization != null && !authorization.isBlank()) {
            request = chain.request()
                    .newBuilder()
                    .addHeader("Authorization", authorization)
                    .build();
        }
        return chain.proceed(request);
    }
}
