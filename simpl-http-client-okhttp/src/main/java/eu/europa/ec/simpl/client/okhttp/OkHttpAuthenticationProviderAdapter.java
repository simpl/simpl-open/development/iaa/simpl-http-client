package eu.europa.ec.simpl.client.okhttp;

import com.google.gson.JsonParser;
import eu.europa.ec.simpl.client.core.AuthenticationProviderConfig;
import eu.europa.ec.simpl.client.core.adapters.AuthenticationProviderAdapter;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import lombok.SneakyThrows;
import okhttp3.*;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.jetbrains.annotations.NotNull;

public class OkHttpAuthenticationProviderAdapter implements AuthenticationProviderAdapter {

    private final OkHttpClient client;

    private final AuthenticationProviderConfig config;

    public OkHttpAuthenticationProviderAdapter(OkHttpClient client, AuthenticationProviderConfig config) {
        this.client = client;
        this.config = config;
    }

    @Override
    public KeyStore getKeyStore() {
        var privateKey = loadPrivateKey(getPrivateKey());
        return loadCredential(getInstalledCredentials(), privateKey);
    }

    @SneakyThrows
    public KeyStore loadCredential(@NotNull InputStream input, PrivateKey privateKey) {
        var keyStore = KeyStore.getInstance("PKCS12");
        var chain = parseCertificates(input);
        keyStore.load(null, null);
        keyStore.setKeyEntry("chain", privateKey, null, chain.toArray(X509Certificate[]::new));
        return keyStore;
    }

    @SneakyThrows
    private List<X509Certificate> parseCertificates(InputStream input) {
        var pems = loadPemObjects(input);
        return pems.stream().map(this::generateCertificate).toList();
    }

    @SneakyThrows
    private X509Certificate generateCertificate(InputStream inputStream) {
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        return (X509Certificate) certFactory.generateCertificate(inputStream);
    }

    @SneakyThrows
    private PrivateKey loadPrivateKey(byte[] decoded) {
        var keySpec = new PKCS8EncodedKeySpec(decoded);
        var keyFactory = KeyFactory.getInstance("EC");
        return keyFactory.generatePrivate(keySpec);
    }

    @SneakyThrows
    public static List<InputStream> loadPemObjects(InputStream inputStream) {
        List<InputStream> pemStreams = new ArrayList<>();
        PemObject pemObject;
        try (PemReader pemReader = new PemReader(new InputStreamReader(inputStream))) {
            while ((pemObject = pemReader.readPemObject()) != null) {
                pemStreams.add(new ByteArrayInputStream(pemObject.getContent()));
            }
        }
        return pemStreams;
    }

    @SneakyThrows
    private InputStream getInstalledCredentials() {
        var path = config.url().resolve("v1/credentials/download");
        var request = new Request.Builder().get().url(path.toString()).build();
        try (Response response = client.newCall(request).execute()) {
            var res = Objects.requireNonNull(response.body()).byteStream();
            return new ByteArrayInputStream(res.readAllBytes());
        }
    }

    @SneakyThrows
    private byte[] getPrivateKey() {
        var path = config.url().resolve("v1/keypairs");
        var request = new Request.Builder().get().url(path.toString()).build();
        try (Response response = client.newCall(request).execute()) {
            var json = Objects.requireNonNull(response.body()).string();
            var obj = JsonParser.parseString(json).getAsJsonObject();
            var encoded = obj.get("privateKey").getAsString();
            return Base64.getDecoder().decode(encoded);
        }
    }

    @SneakyThrows
    @Override
    public String getEphemeralProof() {
        var url = config.url().resolve("v1/agent/ephemeralProof");
        var request = new Request.Builder().get().url(url.toString()).build();
        try (Response response = client.newCall(request).execute()) {
            var json = Objects.requireNonNull(response.body()).string();
            var obj = JsonParser.parseString(json).getAsJsonObject();
            return obj.get("ephemeralProof").getAsString();
        }
    }

    @SneakyThrows
    @Override
    public boolean validateCredential(String credential) {
        var url = config.url().resolve("v1/agent/credentials/validate");
        var body = RequestBody.create(credential, MediaType.parse("application/x-pem-file"));
        var request = new Request.Builder().post(body).url(url.toString()).build();
        try (Response response = client.newCall(request).execute()) {
            return response.isSuccessful();
        }
    }
}
