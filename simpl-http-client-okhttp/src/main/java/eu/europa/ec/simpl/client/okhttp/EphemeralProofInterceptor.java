package eu.europa.ec.simpl.client.okhttp;

import eu.europa.ec.simpl.client.core.EphemeralProofFlow;
import eu.europa.ec.simpl.client.core.adapters.EphemeralProofAdapter;
import eu.europa.ec.simpl.client.core.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.client.core.suppliers.AuthorityUrlSupplier;
import java.io.EOFException;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EphemeralProofInterceptor extends EphemeralProofFlow<OkHttpClient.Builder, Request>
        implements Interceptor {

    // https://sonarsource.atlassian.net/browse/SONARJAVA-5149 S1075 should not raise issues when the uri is a suffix
    private static final String AUTHORITY_EPHEMERAL_PROOF_TARGET = "/identity-api/mtls/token";
    private static final String AUTHORITY_PUBLIC_KEY_TARGET = "/identity-api/mtls/public-key";
    private static final String PARTICIPANT_EPHEMERAL_PROOF_TARGET = "/user-api/mtls/ephemeral-proof";

    private static final List<String> NO_PREFLIGHT_PATHS =
            List.of(AUTHORITY_EPHEMERAL_PROOF_TARGET, AUTHORITY_PUBLIC_KEY_TARGET, PARTICIPANT_EPHEMERAL_PROOF_TARGET);
    private static final Logger log = LoggerFactory.getLogger(EphemeralProofInterceptor.class);

    protected EphemeralProofInterceptor(
            EphemeralProofAdapter ephemeralProofAdapter,
            AuthorityUrlSupplier authorityUrlSupplier,
            OkHttpClient.Builder client) {
        super(ephemeralProofAdapter, authorityUrlSupplier, client);
    }

    @Override
    protected boolean shouldDoPreflight(Request request) {
        return !NO_PREFLIGHT_PATHS.contains(request.url().encodedPath());
    }

    @Override
    protected void sendEphemeralProofToPeer(String participantUrl, String ephemeralProof) {
        var request = new Request.Builder()
                .post(RequestBody.create(ephemeralProof.getBytes(), MediaType.parse("text/plain")))
                .url(participantUrl + PARTICIPANT_EPHEMERAL_PROOF_TARGET)
                .build();
        try (Response response = client.build().newCall(request).execute()) {
            log.info("ephemeral proof send status: {}", response.code());
        } catch (IOException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    @Override
    protected String getEphemeralProofFromAuthority() {
        var body = RequestBody.create(new byte[0], null);
        var request = new Request.Builder()
                .method("POST", body)
                .url(authorityUrlSupplier.getAuthorityUrl() + AUTHORITY_EPHEMERAL_PROOF_TARGET)
                .build();
        return tryDoCall(request);
    }

    @Override
    protected URI getRequestUrl(Request request) {
        return request.url().uri();
    }

    private @NotNull String tryDoCall(Request request) {

        IOException exception = null;

        try {
            return doCall(request);
        } catch (IOException e) {
            exception = e;
        }

        if (exception.getCause() instanceof EOFException) {
            try {
                return doCall(request);
            } catch (IOException e) {
                exception = e;
            }
        }

        throw new RuntimeWrapperException(exception);
    }

    private @NotNull String doCall(Request request) throws IOException {
        try (Response response = client.build().newCall(request).execute()) {
            return response.body().string();
        }
    }

    @NotNull @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        this.execute(chain.request());
        return chain.proceed(chain.request());
    }
}
