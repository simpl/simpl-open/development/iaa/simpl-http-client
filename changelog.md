# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2024-12-02

### Added
- added custom sonar properties
- Hostname verifier works also in `feign` library
- library can now build truststore from keystore

### Changed
- downgraded `java version` to 17

### Fixed
- skipped default hostname verification during SSL handshake
- fix `NullPointerException` on loading keystore without password
- baseurl handling

## [0.7.7] - 2024-12-02

### Fixed
- Load KeyStore and TrustStore from environment variables
- Participant url is now extracted correctly when httpclient target is not only the baseurl
- Fallback and disable trust managers checks
- Fix NullPointerException on loading keystore without password
- HostnameVerifier work also in feign library